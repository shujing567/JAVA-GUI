package com.shujing.domain;

/**
 * @author 陈书憬
 * @version 1.0
 */
public class UserShop {
    private int id;
    private String number;
    private String name;
    private float rent_money;
    private int num;

    public UserShop(String number, String name, float rent_money, int num, int id) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.rent_money = rent_money;
        this.num = num;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public UserShop() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRent_money() {
        return rent_money;
    }

    public void setRent_money(float rent_money) {
        this.rent_money = rent_money;
    }

    @Override
    public String toString() {
        return "UserShop{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", rent_money='" + rent_money + '\'' +
                ", num=" + num +
                '}';
    }
}


