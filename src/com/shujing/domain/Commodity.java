package com.shujing.domain;

/**
 * @author 陈书憬
 * @version 1.0
 * 所有商品 序号 编号 类型 名称 品牌 图片 租金 所在站点 充电宝状态 是否已出租 详细
 */
public class Commodity {
    private Integer id;
    private String number;
    private String types;
    private String name;
    private String brand;
    private float rent_money;
    private String locations;
    private String state;
    private String rent;
    private String input;
    private String output;
    private String size;
    private String energy;
    private String energy_size;
    private String time;
    private int battery;
    private int num;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getEnergy() {
        return energy;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }

    public String getEnergy_size() {
        return energy_size;
    }

    public void setEnergy_size(String energy_size) {
        this.energy_size = energy_size;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Commodity() {
    }

    public Commodity(Integer id, String number, String types, String name, String brand, float rent_money, String locations, String state, String rent, String input, String output, String size, String energy, String energy_size, String time, int battery,int num) {
        this.id = id;
        this.number = number;
        this.types = types;
        this.name = name;
        this.brand = brand;
        this.rent_money = rent_money;
        this.locations = locations;
        this.state = state;
        this.rent = rent;
        this.input = input;
        this.output = output;
        this.size = size;
        this.energy = energy;
        this.energy_size = energy_size;
        this.time = time;
        this.battery = battery;
        this.num = num;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public float getRent_money() {
        return rent_money;
    }

    public void setRent_money(float rent_money) {
        this.rent_money = rent_money;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    @Override
    public String toString() {
        return "Commodity{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", types='" + types + '\'' +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", rent_money=" + rent_money +
                ", locations='" + locations + '\'' +
                ", state='" + state + '\'' +
                ", rent='" + rent + '\'' +
                ", input='" + input + '\'' +
                ", output='" + output + '\'' +
                ", size='" + size + '\'' +
                ", energy='" + energy + '\'' +
                ", energy_size='" + energy_size + '\'' +
                ", time='" + time + '\'' +
                ", battery=" + battery +
                ", num=" + num +
                '}';
    }
}
