package com.shujing.domain;

/**
 * @author 陈书憬
 * @version 1.0
 */
public class MyOrder {
    private int id;
    private String name;
    private float rent_money;
    private int num;
    private String number;
    private String start_time;
    private String end_time;
    private String state;
    private float pay;

    public MyOrder() {
    }

    public MyOrder(int id, String name, float rent_money, int num, String number, String start_time, String end_time, String state, float pay) {
        this.id = id;
        this.name = name;
        this.rent_money = rent_money;
        this.num = num;
        this.number = number;
        this.start_time = start_time;
        this.end_time = end_time;
        this.state = state;
        this.pay = pay;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public float getPay() {
        return pay;
    }

    public void setPay(float pay) {
        this.pay = pay;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRent_money() {
        return rent_money;
    }

    public void setRent_money(float rent_money) {
        this.rent_money = rent_money;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    @Override
    public String toString() {
        return "MyOrder{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", rent_money=" + rent_money +
                ", num=" + num +
                ", number='" + number + '\'' +
                ", start_time='" + start_time + '\'' +
                ", end_time='" + end_time + '\'' +
                ", state='" + state + '\'' +
                ", pay=" + pay +
                '}';
    }
}
