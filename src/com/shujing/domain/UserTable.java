package com.shujing.domain;

/**
 * @author 陈书憬
 * @version 1.0
 * 管理员对应的注册表
 */
public class UserTable { //Javabean, POJO, Domain对象

    private Integer id;
    private String name;
    private String password;


    public UserTable() { //一定要给一个无参构造器[反射需要]
    }


    public UserTable(Integer id, String sname, String spassword) {
        this.id = id;
        this.name = sname;
        this.password = spassword;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserTable{" +
                "id=" + id +
                ", sname='" + name + '\'' +
                ", spassword='" + password + '\'' +
                '}';
    }
}
