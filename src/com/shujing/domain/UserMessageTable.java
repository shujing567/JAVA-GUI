package com.shujing.domain;


/**
 * @author 陈书憬
 * @version 1.0
 * 注册的用户信息，映射
 */
public class UserMessageTable {

    private Integer id;
    private String name;
    private String password;
    private String sex;
    private String address;
    private String birthday;
    private String phone;

    public UserMessageTable() {
    }

    public UserMessageTable(Integer id, String name, String password, String sex, String address, String birthday, String phone) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.sex = sex;
        this.address = address;
        this.birthday = birthday;
        this.phone = phone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "UserMessageTable{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", sex=" + sex +
                ", address='" + address + '\'' +
                ", birthday='" + birthday + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
