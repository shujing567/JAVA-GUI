package com.shujing.view.panels;

import com.shujing.common.Containers;

import javax.swing.*;
import java.awt.*;

/**
 * @author 陈书憬
 * @version 1.0
 * 欢迎用户界面
 */
public class P0_ extends JPanel {
    public P0_(String username) {
        setLayout(new BorderLayout());
        setSize(new Dimension(400, 400));
        setBackground(new Color(236, 236, 236));

        //这一步需要创建一个单独JPanel来存放标签jLabel
        JLabel jLabel = new Containers.JLabel_("欢迎进入充电宝租赁商城！", Font.BOLD, 37, new Color(201, 202, 196));
        add(jLabel);

        setVisible(true);//显示可见
    }
}