package com.shujing.view.panels;

import com.shujing.view.main.UserFrame_;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author 陈书憬
 * @version 1.0
 * 用户界面加载
 */
public class LoadingUser extends JFrame implements Runnable {
    private final MyPanel mp;
    private int index;
    String username1;//用来记录登录页面传来的用户名值

    ImageIcon[] img = new ImageIcon[22];
    public LoadingUser(String username) {
        username1 = username;//保存用户名
        setUndecorated(true);  //设置窗口关闭栏不可见

        //设置界面显示在显示屏中间
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        int INIT_W = 320;//窗体初始宽度
        int INIT_H = 180;//窗体初始高度
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);

        //底部面板
        JPanel bottomJPanel = new JPanel();

        //动画面板
        mp = new MyPanel();
        mp.setSize(new Dimension(640, 500));

        //设置布局
        bottomJPanel.setLayout(new BorderLayout());

        //添加组件
        bottomJPanel.add(mp, BorderLayout.CENTER);
        add(bottomJPanel);

        //遍历所有图片
        for (int i = 0; i < img.length; i++) {
            String imagePath = "src/images/user/" + (i + 21) + ".jpg";
            img[i] = new ImageIcon(imagePath);
        }


        //显示窗口可见
        setVisible(true);

        //每0.15s对图片更换
        Timer timer = new Timer(150, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mp.repaint();    //刷新图片
            }
        });
        timer.start(); //动画线程启动
    }

    @Override
    public void run() {
        for (int i = 0; i <= 120; i++) {
            try {
                Thread.sleep(27);//睡眠2.7秒，即加载进度条的时间，这里为2.7秒
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        dispose();   //关闭当前资源，即加载界面
        new UserFrame_(username1); //开启用户界面，并且传入用户名
    }

    class MyPanel extends JPanel {
        @Override
        public void paint(Graphics g) {
            super.paint(g);//调用父类的绘图方法
            g.drawImage(img[index % img.length].getImage(), 0, 0, this);   // 绘制图像
            index++;    // 更新图像索引，用于循环播放多张图像
        }
    }
        public static void main(String[] args) {
        LoadingUser loadingUser = new LoadingUser("1");
        //对该类开启线程
        new Thread(loadingUser).start();
    }
}
