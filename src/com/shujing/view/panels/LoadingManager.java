package com.shujing.view.panels;

import com.shujing.view.main.MainFrame_;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author 陈书憬
 * @version 1.0
 * 管理员登录加载页面
 */
public class LoadingManager extends JFrame implements Runnable {
    private final MyPanel mp;
    private int index;
    String username1;
    public JProgressBar progressBar;


    ImageIcon[] img = new ImageIcon[20];

    public LoadingManager(String username) {
        username1 = username;//存储传过来的用户名
        setUndecorated(true); //设置窗口关闭栏不可见

        //设置界面显示在显示屏中间
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        int INIT_W = 640;   //窗体初始宽度
        int INIT_H = 580;  //窗体初始高度
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);

        //底部面板
        JPanel bottomJPanel = new JPanel();

        //动画面板
        mp = new MyPanel();
        mp.setSize(new Dimension(640, 500));

        //进度条面板
        JPanel southJPanel = new JPanel();
        southJPanel.setBackground(new Color(0, 0, 0));
        progressBar = new JProgressBar();//创建进度条
        progressBar.setStringPainted(true);  //显示当前进度信息
        progressBar.setBorderPainted(false);   //设置进度条边框不显示
        progressBar.setForeground(new Color(0, 0, 0));  //前景色
        progressBar.setBackground(new Color(2, 2, 2));     //后景色
        progressBar.setOpaque(true);
        southJPanel.add(progressBar);

        //设置布局
        bottomJPanel.setLayout(new BorderLayout());
        bottomJPanel.add(mp, BorderLayout.CENTER);
        bottomJPanel.add(southJPanel, BorderLayout.SOUTH);

        //添加组件
        add(bottomJPanel);

        //遍历所有图片
        for (int i = 0; i < img.length; i++) {
            String imagePath = "src/images/manager/" + (i + 1) + ".jpg";
            img[i] = new ImageIcon(imagePath);
        }
        //显示窗口可见
        setVisible(true);

        //每0.1s对图片更换
        Timer timer = new Timer(100, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //刷新图片
                mp.repaint();
            }
        });
        //动画线程启动
        timer.start();
    }

    @Override
    public void run() {
        for (int i = 0; i <= 120; i++) {
            try {
                Thread.sleep(30);//加载进度条的时间，这里为3秒
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            progressBar.setValue(i);
        }
        //关闭当前资源，即加载界面
        dispose();
        //开启主界面
        new MainFrame_(username1);
    }

    class MyPanel extends JPanel {
        @Override
        public void paint(Graphics g) {
            //调用父类的绘图方法
            super.paint(g);
            // 绘制图像
            g.drawImage(img[index % img.length].getImage(), 0, 0, this);
            // 更新图像索引，用于循环播放多张图像
            index++;
        }
    }

    public static void main(String[] args) {
        LoadingManager loadingManager = new LoadingManager("");
        //对该类开启线程
        new Thread(loadingManager).start();
    }
}
