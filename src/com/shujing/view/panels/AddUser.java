package com.shujing.view.panels;

import com.shujing.common.Containers;
import com.shujing.dao.UserMessageTableDAO;
import com.shujing.domain.UserMessageTable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * @author 陈书憬
 * @version 1.0
 * 管理员添加商品界面
 */
public class AddUser extends JFrame implements Containers, ActionListener {
    JButton jButton1, jButton2;
    boolean loop = false;
    TextField_ textField2, textField3, textField4, textField5, textField6, textField7, textField8, textField9;
    Integer id = null;
    UserMessageTableDAO userMessageTableDAO = new UserMessageTableDAO();

    public AddUser() {
        setTitle("添加用户");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        //设置界面在屏幕居中
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        int INIT_W = 440; //窗体初始宽度
        int INIT_H = 600;  //窗体初始高度
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);

        //底部面板
        JPanel_ jPanelMain = new JPanel_(0, 0, Color.white);
        setContentPane(jPanelMain);
        jPanelMain.setLayout(new BoxLayout(jPanelMain, BoxLayout.Y_AXIS));

        JPanel jPanel1 = new JPanel();
        JLabel_ jLabel1 = new JLabel_("请输入需要添加用户的信息", 0, 17, null);
        jPanel1.add(jLabel1);

        JPanel jPanel2 = new JPanel();
        JLabel_ jLabel2 = new JLabel_("用  户  名: ", 0, 13, null);
        textField2 = new TextField_("", 20);
        jPanel2.add(jLabel2);
        jPanel2.add(textField2);

        JPanel jPanel3 = new JPanel();
        JLabel_ jLabel3 = new JLabel_("密       码: ", 0, 13, null);
        textField3 = new TextField_("", 20);
        jPanel3.add(jLabel3);
        jPanel3.add(textField3);

        JPanel jPanel4 = new JPanel();
        JLabel_ jLabel4 = new JLabel_("性       别: ", 0, 13, null);
        textField4 = new TextField_("", 20);
        jPanel4.add(jLabel4);
        jPanel4.add(textField4);

        JPanel jPanel5 = new JPanel();
        JLabel_ jLabel5 = new JLabel_("家庭住址: ", 0, 13, null);
        textField5 = new TextField_("", 20);
        jPanel5.add(jLabel5);
        jPanel5.add(textField5);

        JPanel jPanel6 = new JPanel();
        JLabel_ jLabel6 = new JLabel_("出生日期: ", 0, 13, null);
        textField6 = new TextField_("", 20);
        jPanel6.add(jLabel6);
        jPanel6.add(textField6);

        JPanel jPanel7 = new JPanel();
        JLabel_ jLabel7 = new JLabel_("手机号： ", 0, 13, null);
        textField7 = new TextField_("", 20);
        jPanel7.add(jLabel7);
        jPanel7.add(textField7);


        JPanel jPanel10 = new JPanel();
        jButton1 = new JButton("添加");
        jButton2 = new JButton("返回");
        jPanel10.add(jButton1);
        jPanel10.add(jButton2);

        //设置按钮背景为透明
        jButton1.setContentAreaFilled(false);
        jButton2.setContentAreaFilled(false);

        add(jPanel1);
        add(jPanel2);
        add(jPanel3);
        add(jPanel4);
        add(jPanel5);
        add(jPanel6);
        add(jPanel7);
        add(jPanel10);

        jButton2.addActionListener(this);
        jButton1.addActionListener(this);

        setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jButton2) {
            dispose();
        }
        if (e.getSource() == jButton1) {
            String text2 = textField2.getText();
            String text3 = textField3.getText();
            String text4 = textField4.getText();
            String text5 = textField5.getText();
            String text6 = textField6.getText();
            String text7 = textField7.getText();


            List<UserMessageTable> userMessageTableList = userMessageTableDAO.queryMulti("select * from user_message_table where id >= ?", UserMessageTable.class, 1);
            for (UserMessageTable userMessageTable : userMessageTableList) {
                //获取集合中的id总数
                id = userMessageTable.getId() + 1;
            }
            if (text2.isEmpty() || text3.isEmpty() || text4.isEmpty() || text5.isEmpty() || text6.isEmpty() || text7.isEmpty()) {
                JOptionPane.showMessageDialog(null, "请输入用户信息！");
            } else {
                int update = userMessageTableDAO.update("insert into user_message_table values(?,?,?,?,?,?,?)", id, text2, text3, text4, text5, text6, text7);
                if (update > 0) {
                    loop = true;
                } else {
                    JOptionPane.showMessageDialog(null, "添加失败！");
                }
                if (update > 0) {
                    JOptionPane.showMessageDialog(null, "用户信息添加成功！");
                    loop = true;
                } else {
                    JOptionPane.showMessageDialog(null, "用户信息添加失败...");
                    loop = true;
                }
            }
        }
    }

    public static void main(String[] args) {
        new AddUser();
    }
}
