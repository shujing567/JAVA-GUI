package com.shujing.view.panels;

import com.shujing.common.Containers;
import com.shujing.dao.UserMessageTableDAO;
import com.shujing.domain.UserMessageTable;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * @author 陈书憬
 * @version 1.0
 * 管理员管理注册用户界面
 */
public class P2 extends JPanel implements ActionListener {
    JButton add,updButton,find;
    private final DefaultTableModel tableModel;   // 定义表格模型对象
    private final JTable table;   // 表格对象
    UserMessageTableDAO userMessageTableDAO = new UserMessageTableDAO();

    public P2() {
        setLayout(new BorderLayout());
        JLabel jLabel = new Containers.JLabel_("注册用户信息", Font.BOLD, 25, null);
        add(jLabel, BorderLayout.NORTH);

        // 创建表头
        String[] columnNames = {"学号", "用户名", "密码", "性别", "家庭住址", "出生日期", "手机号"};
        List<UserMessageTable> userMessageTableList = userMessageTableDAO.queryMulti("select * from user_message_table where id >= ?", UserMessageTable.class, 1);

        // 将List<UserMessageTable>转换为String[][]
        String[][] tableValues = new String[userMessageTableList.size()][7]; // 假设每个userMessageTable对象有7个属性
        for (int i = 0; i < userMessageTableList.size(); i++) {
            UserMessageTable userMessageTable = userMessageTableList.get(i);
            tableValues[i][0] = userMessageTable.getId().toString();
            tableValues[i][1] = userMessageTable.getName();
            tableValues[i][2] = userMessageTable.getPassword();
            tableValues[i][3] = userMessageTable.getSex();
            tableValues[i][4] = userMessageTable.getAddress();
            tableValues[i][5] = userMessageTable.getBirthday();
            tableValues[i][6] = userMessageTable.getPhone();
        }
        tableModel = new DefaultTableModel(tableValues, columnNames);
        table = new JTable(tableModel);

        // 设置单元格内容居中对齐
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        table.setDefaultRenderer(Object.class, centerRenderer);
        table.setEnabled(true);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);

        JPanel panel = new JPanel();
        add(panel, BorderLayout.SOUTH);

        // 删除按钮
         add = new JButton("添加用户");
        JButton delButton = new JButton("注销用户信息");
        JButton update = new JButton("修改用户信息");
        updButton = new JButton("刷新");
        find = new JButton("查找");

        delButton.setContentAreaFilled(false);
        add.setContentAreaFilled(false);
        update.setContentAreaFilled(false);
        updButton.setContentAreaFilled(false);
        find.setContentAreaFilled(false);


        panel.add(add);
        panel.add(delButton);
        panel.add(update);
        panel.add(updButton);
        panel.add(find);
        add.addActionListener(this);

        //删除数据
        delButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int selectedRow = table.getSelectedRow();
                if (selectedRow != -1) {
                    int result = JOptionPane.showConfirmDialog(null, "确定要删除该条用户信息吗？");

                    if (result == JOptionPane.YES_OPTION) { // 点击了“是”按钮
                        tableModel.removeRow(selectedRow);  // 从表格模型中删除选择的行
                        UserMessageTable userMessageTable = userMessageTableList.get(selectedRow);    // 从数据库中删除对应的数据
                        int id = userMessageTable.getId();
                        int update = userMessageTableDAO.update("delete from user_message_table where id = ?", id);
                        System.out.println(update > 0 ? "删除成功！" : "删除失败！");

                        JOptionPane.showMessageDialog(null, "删除成功！");
                    } else if (result == JOptionPane.NO_OPTION) {  // 点击了“否”按钮
                    } else {    // 点击了“取消”按钮
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "请选择要删除的用户！");
                }
            }
        });
        // 刷新按钮事件
        updButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 重新查询数据库
                List<UserMessageTable> userMessageTableList1 = userMessageTableDAO.queryMulti("select * from user_message_table where id >= ?", UserMessageTable.class, 1);
                tableModel.setRowCount(0);  // 更新表格数据模型
                for (int i = 0; i < userMessageTableList1.size(); i++) {
                    UserMessageTable userMessageTable = userMessageTableList1.get(i);
                    String[] rowData = new String[7];
                    rowData[0] = userMessageTable.getId().toString();
                    rowData[1] = userMessageTable.getName();
                    rowData[2] =  userMessageTable.getPassword();
                    rowData[3] = userMessageTable.getSex();
                    rowData[4] = userMessageTable.getAddress();
                    rowData[5] = userMessageTable.getBirthday();
                    rowData[6] = userMessageTable.getPhone();

                    tableModel.addRow(rowData);
                }
                // 刷新表格界面
                table.repaint();
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==add){
            new AddUser();
        }
    }
}