package com.shujing.view.panels;

import com.shujing.common.Containers;
import com.shujing.dao.MyOrderDAO;
import com.shujing.dao.UserShopDAO;
import com.shujing.domain.MyOrder;
import com.shujing.domain.UserShop;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author 陈书憬
 * @version 1.0
 * 购物车界面
 */

public class P6 extends JPanel {
    private final DefaultTableModel tableModel;   // 定义表格模型对象
    private final JTable table;   // 表格对象
    JLabel jLabel1;  //显示总价
    UserShopDAO userShopDAO = new UserShopDAO();
    float rentMoney = 0; //显示总租金
    MyOrderDAO myOrderDAO;
    int id;
    int i;

    public P6() {
        setLayout(new BorderLayout());
        JLabel jLabel = new Containers.JLabel_("购物车", Font.BOLD, 25, null);
        add(jLabel, BorderLayout.NORTH);
        jLabel1 = new JLabel("总价：￥0元");
        // 创建表头
        String[] columnNames = {"序号", "名称", "编号", "租金", "数量"};
        List<UserShop> userShopList = userShopDAO.queryMulti("select * from user_shop_table where id >= ?", UserShop.class, 1);
        for (UserShop userShops : userShopList) {
            rentMoney += userShops.getRent_money();//取出购物车租金总额
        }
        jLabel1.setText("总价：￥  " + rentMoney + "  元");
        // 将List<UserShop>的值取出赋值给String[][] tableValues数组
        String[][] tableValues = new String[userShopList.size()][5]; // 假设每个UserTable对象有3个属性
        for (int i = 0; i < tableValues.length; i++) {
            UserShop userShops = userShopList.get(i);
            tableValues[i][0] = String.valueOf(userShops.getId());//获取每个id
            tableValues[i][1] = userShops.getName();//获取每个名称
            tableValues[i][2] = userShops.getNumber();//获取每个名称
            tableValues[i][3] = userShops.getRent_money() + "/h";//获取每条租金
            tableValues[i][4] = String.valueOf(userShops.getNum());//
        }

        tableModel = new DefaultTableModel(tableValues, columnNames);//将表头和数据放入表模型中
        table = new JTable(tableModel);//根据表模型创建表

        // 设置单元格内容居中对齐
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        table.setDefaultRenderer(Object.class, centerRenderer);
        table.setEnabled(true);//设置表不可编辑

        JScrollPane scrollPane = new JScrollPane(table);//在表中设置滚轮
        add(scrollPane, BorderLayout.CENTER);

        JPanel panel = new JPanel();//底部面板 用来放置刷新 删除商品和标签的面板
        add(panel, BorderLayout.SOUTH);

        JButton updButton = new JButton("刷新");
        updButton.setContentAreaFilled(false);//设置按钮背景透明

        JButton delButton = new JButton("删除商品");
        delButton.setContentAreaFilled(false);//设置按钮背景透明

        JButton rent = new JButton("租赁");
        rent.setContentAreaFilled(false);//设置按钮背景透明


        //添加按钮
        panel.add(updButton);
        panel.add(delButton);
        panel.add(rent);
//        panel.add(jLabel1);

        //删除数据
        delButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();//获取鼠标选择的行
                if (selectedRow != -1) { //！=-1 表示选择了表中的某一内容
                    int result = JOptionPane.showConfirmDialog(null, "确定要删除该商品吗？");
                    if (result == JOptionPane.YES_OPTION) {
                        UserShop userShop = userShopList.get(selectedRow);
                        int id = userShop.getId();//获取相应数据库表对应id
                        int update = userShopDAO.update("delete from user_shop_table where id = ?", id);
                        if (update > 0) {
                            // 成功删除数据库中的数据后，从表格模型和用户列表中移除数据
                            tableModel.removeRow(selectedRow);
                            userShopList.remove(selectedRow); // 这里删除数据库中对应的元素
                            JOptionPane.showMessageDialog(null, "删除成功！");
                            // 更新总价
                            rentMoney -= userShop.getRent_money();
                            jLabel1.setText("总价：￥" + rentMoney + "元");
                        } else {
                            JOptionPane.showMessageDialog(null, "删除失败！");
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "请选择要删除的商品信息！");
                }
            }
        });

        // 刷新按钮事件
        updButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 重新查询数据库
                userShopList.clear(); // 清空当前列表
                List<UserShop> freshData = userShopDAO.queryMulti("select * from user_shop_table where id >= ?", UserShop.class, 1);
                userShopList.addAll(freshData); // 用最新数据填充列表
                // 更新表格数据模型
                tableModel.setRowCount(0); // 清空表格模型的所有行
                rentMoney = 0; // 重置总租金
                for (UserShop userShop : userShopList) {
                    String[] rowData = {
                            String.valueOf(userShop.getId()),
                            userShop.getName(),
                            userShop.getNumber(),
                            userShop.getRent_money() + "/h",
                            String.valueOf(userShop.getNum())};//

                    tableModel.addRow(rowData); // 向表格模型添加新行
//                    rentMoney += Float.parseFloat(userShop.getRent_money()); // 累加总租金
                }

            }
        });
        rent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();//获取鼠标选择的行
                if (selectedRow != -1) { //！=-1 表示选择了表中的某一内容
                    int result = JOptionPane.showConfirmDialog(null, "租赁后按每小时规定金额收费，请确定租赁！");
                    if (result == JOptionPane.YES_OPTION) {
                        UserShop userShop = userShopDAO.querySingle("select * from user_shop_table where id = ?", UserShop.class, (selectedRow + 1));
                        System.out.println("选择的行数是：" + (selectedRow + 1));
                        String name = userShop.getName();
                        System.out.println(name);

                        String number = userShop.getNumber();
                        float rentMoney1 = userShop.getRent_money();
                        int num = userShop.getNum();

                        //记录当前租借时间
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        Date date = new Date();
                        String currentDate = formatter.format(date);
                        System.out.println(currentDate);

                        myOrderDAO = new MyOrderDAO();


                        //遍历我的订单 看看有没有这个数据
                        MyOrder myOrder = myOrderDAO.querySingle("select * from my_order where name = ?", MyOrder.class, name);


                        int startId = 0;
                       myOrderDAO.update("insert into my_order values(?, ?, ?, ?, ?,?,null,null,null)", startId, name, rentMoney1, num, number, currentDate);

                        //遍历整张不表 找出id最大的数
                        List<MyOrder> myOrders = myOrderDAO.queryMulti("select * from my_order where id >= ?", MyOrder.class, 1);
                        for (MyOrder myOrder1 : myOrders) {
                            i = myOrder1.getId();
                            System.out.println("循环里面的i："+i);
                        }
                        i++;
                        System.out.println("外面的i："+i);

                        myOrderDAO.update("UPDATE my_order SET id = ? WHERE id = ?", i, startId);

                        // 成功删除数据库中的数据后，从表格模型和用户列表中移除数据
                        tableModel.removeRow((selectedRow));
                        userShopList.remove(selectedRow); // 这里删除数据库中对应的元素

                        int update = userShopDAO.update("delete from user_shop_table where id = ?", (selectedRow+1));
                    }
                }
            }
        });
    }
}