package com.shujing.view.panels;

import com.shujing.common.Containers;
import com.shujing.dao.CommodityDAO;
import com.shujing.domain.Commodity;
import com.shujing.view.img_panels.Commodity_msg;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * @author 陈书憬
 * @version 1.0
 * 管理员商品信息表
 */
public class P3 extends JPanel {
    private final DefaultTableModel tableModel;   // 定义表格模型对象
    private final JTable table;   // 表格对象
    CommodityDAO commodityDAO = new CommodityDAO();
    Commodity commodity;

    public P3() {
        setLayout(new BorderLayout());

        JLabel jLabel = new Containers.JLabel_("充电宝信息", Font.BOLD, 25, null);
        add(jLabel, BorderLayout.NORTH);

        // 创建表头
        String[] columnNames = {"序号", "编号", "类型", "名称", "品牌", "租金", "所在站点", "充电宝状态", "是否已出租", "当前电量","剩余库存"};
        java.util.List<Commodity> commodityList = commodityDAO.queryMulti("select * from commodity_table where id >= ?", Commodity.class, 1);

        // 将List<UserTable>转换为String[][]
        String[][] tableValues = new String[commodityList.size()][11]; // 假设每个commodity对象有10个属性
        for (int i = 0; i < commodityList.size(); i++) {
            Commodity commodity = commodityList.get(i);
            tableValues[i][0] = commodity.getId().toString();
            tableValues[i][1] = commodity.getNumber();
            tableValues[i][2] = commodity.getTypes();
            tableValues[i][3] = commodity.getName();
            tableValues[i][4] = commodity.getBrand();
            tableValues[i][5] = String.valueOf(commodity.getRent_money());
            tableValues[i][6] = commodity.getLocations();
            tableValues[i][7] = commodity.getState();
            tableValues[i][8] = commodity.getRent();
            tableValues[i][9] = commodity.getBattery() + "%";
            tableValues[i][10] = String.valueOf(commodity.getNum());
        }
        tableModel = new DefaultTableModel(tableValues, columnNames);
        table = new JTable(tableModel);

        // 设置单元格内容居中对齐
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        table.setDefaultRenderer(Object.class, centerRenderer);
        table.setEnabled(true);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);

        JPanel panel = new JPanel();
        add(panel, BorderLayout.SOUTH);

        // 增删改三个按钮
        JButton go = new JButton("上架");
        JButton down = new JButton("下架");
        JButton addButton = new JButton("添加商品");
        JButton updButton = new JButton("刷新");
        JButton delButton = new JButton("删除");
        JButton detail = new JButton("详细");
        JButton update = new JButton("调整库存");
        JButton charge = new JButton("充电");

        //设置按钮背景为透明
        go.setContentAreaFilled(false);
        down.setContentAreaFilled(false);
        addButton.setContentAreaFilled(false);
        updButton.setContentAreaFilled(false);
        delButton.setContentAreaFilled(false);
        detail.setContentAreaFilled(false);
        update.setContentAreaFilled(false);
        charge.setContentAreaFilled(false);

        panel.add(go);
        panel.add(down);
        panel.add(addButton);
        panel.add(updButton);
        panel.add(delButton);
        panel.add(detail);
        panel.add(update);
        panel.add(charge);

        // 添加按钮事件
        addButton.addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            new AddCommodity();
                                        }
                                    }
        );

        //删除数据
        delButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();
                System.out.println(selectedRow);
                if (selectedRow != -1) {
                    int result = JOptionPane.showConfirmDialog(null, "确定要删除该条商品信息吗？");
                    if (result == JOptionPane.YES_OPTION) { // 点击了“是”按钮
                        tableModel.removeRow(selectedRow);// 从表格模型中删除选择的行
                        Commodity commodity = commodityList.get(selectedRow); // 从数据库中删除对应的数据
                        int id = commodity.getId();
                        int update = commodityDAO.update("delete from commodity_table where id = ?", id);
                        System.out.println(update > 0 ? "删除成功！" : "删除失败！");
                        JOptionPane.showMessageDialog(null, "删除成功！");
                    } else if (result == JOptionPane.NO_OPTION) {   // 点击了“否”按钮
                    } else {  // 点击了“取消”按钮
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "请选择要删除的商品！");
                }
            }

        });
        // 刷新按钮事件
        updButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 重新查询数据库
                java.util.List<Commodity> commodityList = commodityDAO.queryMulti("select * from commodity_table where id >= ?", Commodity.class, 1);
                tableModel.setRowCount(0);  // 更新表格数据模型
                for (int i = 0; i < commodityList.size(); i++) {
                    Commodity commodity = commodityList.get(i);
                    String[] rowData = new String[11];
                    rowData[0] = commodity.getId().toString();
                    rowData[1] = commodity.getNumber();
                    rowData[2] = commodity.getTypes();
                    rowData[3] = commodity.getName();
                    rowData[4] = commodity.getBrand();
                    rowData[5] = String.valueOf(commodity.getRent_money());
                    rowData[6] = commodity.getLocations();
                    rowData[7] = commodity.getState();
                    rowData[8] = commodity.getRent();
                    rowData[9] = commodity.getBattery() + "%";
                    rowData[10] = String.valueOf(commodity.getNum());
                    tableModel.addRow(rowData);
                }
                // 刷新表格界面
                table.repaint();
            }
        });

        //充电宝详细
        detail.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow() + 1;
                if (selectedRow != -1) {
                    Commodity commodity = commodityDAO.querySingle("select * from commodity_table where id = ?", Commodity.class, selectedRow);
                    String name = commodity.getName();
                    System.out.println(name);

                    new Commodity_msg(name, selectedRow).setVisible(true);

                } else {
                    JOptionPane.showMessageDialog(null, "请选择要查询的商品！");
                }
            }

        });

        charge.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow() + 1;
                if (selectedRow != -1) {
                     commodity = commodityDAO.querySingle("select * from commodity_table where id = ?", Commodity.class, selectedRow);
                    int battery = commodity.getBattery();
                    if (battery >= 100) {
                        JOptionPane.showMessageDialog(null, "电量已经充满！");
                    } else {
                        int result = commodity.getBattery() + 1;
                        int charge = commodityDAO.update("UPDATE commodity_table SET battery = ? WHERE id = ?", result, selectedRow);

                    }
                } else{
                    JOptionPane.showMessageDialog(null, "请选择要充电的充电宝！");
                }
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow() + 1;
                if (selectedRow != -1) {
                    Commodity commodity = commodityDAO.querySingle("select * from commodity_table where id = ?", Commodity.class, selectedRow);


                      String name = commodity.getName();
                      new UpdateNum(name);


                } else {
                    JOptionPane.showMessageDialog(null, "请选择要调整库存充电宝！");
                }

            }
        });
    }
}