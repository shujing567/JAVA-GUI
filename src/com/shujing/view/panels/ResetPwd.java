package com.shujing.view.panels;

import com.shujing.dao.UserMessageTableDAO;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author 陈书憬
 * @version 1.0
 * 个人信息->修改密码界面
 */
public class ResetPwd extends JFrame implements ActionListener {
    JButton jButton1, jButton2;
    TextField textField1, textField2;
    UserMessageTableDAO userMessageTableDAO = new UserMessageTableDAO();
    String username1;

    public ResetPwd(String username) {
        username1 = username;
        setTitle("修改密码");

        //设置界面在屏幕居中
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        int INIT_W = 320; //窗体初始宽度
        int INIT_H = 200;    //窗体初始高度
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new FlowLayout());

        JPanel jPanel0 = new JPanel();
        JLabel jLabel0 = new JLabel("用户：" + username);
        jPanel0.add(jLabel0);

        JPanel jPanel1 = new JPanel();
        JLabel jLabel = new JLabel("密        码： ");
        textField1 = new TextField(15);
        jPanel1.add(jLabel);
        jPanel1.add(textField1);

        JPanel jPanel2 = new JPanel();
        JLabel jLabel2 = new JLabel("确认密码： ");
        textField2 = new TextField(15);
        jPanel2.add(jLabel2);
        jPanel2.add(textField2);

        JPanel jPanel3 = new JPanel();
        jButton1 = new JButton("确认修改");
        jButton2 = new JButton("返回");
        jPanel3.add(jButton1);
        jPanel3.add(jButton2);

        add(jPanel0);
        add(jPanel1);
        add(jPanel2);
        add(jPanel3);

        jButton1.addActionListener(this);
        jButton2.addActionListener(this);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jButton1) {
            String pwd1 = textField1.getText();
            String pwd2 = textField2.getText();
            if (pwd1.isEmpty() || pwd2.isEmpty()) {
                JOptionPane.showMessageDialog(null, "请输入要修改的密码！");
            } else {
                if (pwd1.equals(pwd2)) {
                    //点击按钮后 查询对应名称的数据库信息  user_message_table
                    userMessageTableDAO.update("UPDATE user_message_table SET password = ? WHERE name = ?", pwd1, username1);
                    JOptionPane.showMessageDialog(null, "修改成功！");
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "两次输入密码不一致！");
                }
            }
        } else if (e.getSource() == jButton2) {
            dispose();
        }
    }
}
