package com.shujing.view.panels;

import com.shujing.common.Containers;

import javax.swing.*;
import java.awt.*;

/**
 * @author 陈书憬
 * @version 1.0
 * 管理员欢迎界面
 */
public class P0 extends JPanel {
    public P0() {
        setLayout(new BorderLayout());
        setSize(new Dimension(400, 400));
        setBackground(new Color(236, 236, 236));

        JLabel jLabel = new Containers.JLabel_("欢迎进入充电宝租赁系统！", Font.BOLD, 37, new Color(201, 202, 196));

        add(jLabel);

        setVisible(true);
    }
}