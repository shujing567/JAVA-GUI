package com.shujing.view.panels;

import com.shujing.common.Containers;
import com.shujing.dao.CommodityDAO;
import com.shujing.dao.UserShopDAO;
import com.shujing.domain.Commodity;
import com.shujing.domain.UserShop;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


/**
 * @author 陈书憬
 * @version 1.0
 * 用户商品信息
 */
public class P5 extends JPanel {

    private DefaultTableModel tableModel;   // 定义表格模型对象

    private JTable table;   // 表格对象
    private JButton addButton, updButton;    // 增删改三个按钮
    CommodityDAO commodityDAO = new CommodityDAO();
    JLabel jLabel1;
    int num;//用来记录商品数量
    int num1 = 1;

    public P5() {
        setLayout(new BorderLayout());

        JLabel jLabel = new Containers.JLabel_("商品信息", Font.BOLD, 25, null);

        add(jLabel, BorderLayout.NORTH);

        // 创建表头
        String[] columnNames = {"序号", "编号", "类型", "名称", "品牌", "租金", "所在站点", "充电宝状态", "是否已出租", "当前电量", "剩余数量"};
        List<Commodity> commodityList = commodityDAO.queryMulti("select * from commodity_table where id >= ?", Commodity.class, 1);

        // 将List<UserTable>转换为String[][]
        String[][] tableValues = new String[commodityList.size()][11]; // 假设每个UserTable对象有3个属性
        for (int i = 0; i < commodityList.size(); i++) {
            Commodity commodity = commodityList.get(i);
            tableValues[i][0] = commodity.getId().toString();
            tableValues[i][1] = commodity.getNumber();
            tableValues[i][2] = commodity.getTypes();
            tableValues[i][3] = commodity.getName();
            tableValues[i][4] = commodity.getBrand();
            tableValues[i][5] = commodity.getRent_money() + "/h";
            tableValues[i][6] = commodity.getLocations();
            tableValues[i][7] = commodity.getState();
            tableValues[i][8] = commodity.getRent();
            tableValues[i][9] = commodity.getBattery() + "%";
            tableValues[i][10] = String.valueOf(commodity.getNum());
        }
        tableModel = new DefaultTableModel(tableValues, columnNames);
        table = new JTable(tableModel);

        // 设置单元格内容居中对齐
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        table.setDefaultRenderer(Object.class, centerRenderer);
        table.setEnabled(true);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);

        final JPanel panel = new JPanel();
        add(panel, BorderLayout.SOUTH);

        addButton = new JButton("添加商品至购物车");
        updButton = new JButton("刷新");
        jLabel1 = new JLabel("添加到购物车的商品数量为：0件");

        //设置按钮背景为透明
        addButton.setContentAreaFilled(false);
        updButton.setContentAreaFilled(false);

        panel.add(addButton);
        panel.add(updButton);
        panel.add(jLabel1);

        // 添加按钮事件
        addButton.addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            //获取选中表的内容
                                            int selectedRow = table.getSelectedRow() + 1;
                                            UserShopDAO userShopDAO = new UserShopDAO();
                                            int id = 0;//遍历原来购物车的内容，用来给id赋值
                                            String name;
                                            String rentMoney;
                                            if (selectedRow != -1) {
                                                //获取选中商品单行数据
                                                Commodity commodity = commodityDAO.querySingle("select * from commodity_table where id = ?", Commodity.class, selectedRow);
                                                int battery = commodity.getBattery();
                                                Integer id1 = commodity.getId();
                                                if (commodity != null) {

                                                    name = commodity.getName();//获取对应商品名
                                                    num = commodity.getNum();
                                                    System.out.println("剩余库存" + (num - 1));
                                                    rentMoney = String.valueOf(commodity.getRent_money()); //获取对应商品租金

                                                    if (num <= 0) {
                                                        JOptionPane.showMessageDialog(null, "暂时无库存！");
                                                    } else {
                                                        if (battery >= 50) {
                                                            int num3 = --num;
                                                            //在用户表中插入选中的数据
                                                            commodity = commodityDAO.querySingle("select * from commodity_table where id = ?", Commodity.class, selectedRow);
                                                            String name1 = commodity.getName();
                                                            int num2 = commodity.getNum();
                                                            //遍历购物车表 看看有没有这个数据
                                                            UserShop userShop = userShopDAO.querySingle("select * from user_shop_table where name = ?", UserShop.class, name1);
                                                            if (userShop == null) {
                                                                String number = commodity.getNumber();
                                                                userShopDAO.update("insert into user_shop_table values(?,?,?,?,?)", id1, name1, rentMoney, num2, number);
                                                                userShopDAO.update("UPDATE user_shop_table SET num = ? WHERE name = ?", 1, name1);
                                                            }
                                                            int i = 0;
                                                            if (userShop != null) {
                                                                i = userShop.getNum();
                                                            }
                                                            i++;
                                                            userShopDAO.update("UPDATE user_shop_table SET num = ? WHERE name = ?", i, name);
                                                            commodityDAO.update("UPDATE commodity_table SET num = ? WHERE name = ?", num3, name1);
                                                            System.out.println("添加成功！");
                                                            jLabel1.setText("添加到购物车的商品数量为：" + (num1++) + "件"); // 更新 JLabel 的文本
                                                        } else {
                                                            JOptionPane.showMessageDialog(null, "该商品电量小于50%！");
                                                        }
                                                    }
                                                } else {
                                                    JOptionPane.showMessageDialog(null, "请选择要添加的商品！");
                                                }
                                            }
                                        }
                                    }
        );

        // 刷新按钮事件
        updButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 重新查询数据库
                List<Commodity> commodityList = commodityDAO.queryMulti("select * from commodity_table where id >= ?", Commodity.class, 1);
                // 更新表格数据模型
                tableModel.setRowCount(0);
                for (int i = 0; i < commodityList.size(); i++) {//清空表格后再重新添加表格
                    Commodity commodity = commodityList.get(i);
                    String[] rowData = new String[11];
                    rowData[0] = commodity.getId().toString();
                    rowData[1] = commodity.getNumber();
                    rowData[2] = commodity.getTypes();
                    rowData[3] = commodity.getName();
                    rowData[4] = commodity.getBrand();
                    rowData[5] = commodity.getRent_money() + "/h";
                    rowData[6] = commodity.getLocations();
                    rowData[7] = commodity.getState();
                    rowData[8] = commodity.getRent();
                    rowData[9] = commodity.getBattery() + "%";
                    rowData[10] = String.valueOf(commodity.getNum());
                    tableModel.addRow(rowData);//在界面添加行
                }
                table.repaint(); // 刷新表格界面
            }
        });
    }
}