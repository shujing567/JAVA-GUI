package com.shujing.view.panels;

import com.shujing.common.Containers;
import com.shujing.dao.UserTableDAO;
import com.shujing.domain.UserTable;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author 陈书憬
 * @version 1.0
 * 管理员信息管理界面
 */
public class P1 extends JPanel {
    private final DefaultTableModel tableModel;   // 定义表格模型对象
    private final JTable table;   // 表格对象
    private final JTextField aTextField, bTextField;  // 面板下面的两个输入框
    UserTableDAO userTableDAO = new UserTableDAO();

    public P1() {
        setLayout(new BorderLayout());

        JLabel jLabel = new Containers.JLabel_("管理员信息", Font.BOLD, 25, null);

        add(jLabel, BorderLayout.NORTH);

        // 创建表头
        String[] columnNames = {"编号", "用户名", "密码"};
        List<UserTable> userTableList = userTableDAO.queryMulti("select * from user_table where id >= ?", UserTable.class, 1);

        // 将List<UserTable>转换为String[][]
        String[][] tableValues = new String[userTableList.size()][3]; // 假设每个UserTable对象有3个属性
        for (int i = 0; i < userTableList.size(); i++) {
            UserTable userTable = userTableList.get(i);
            tableValues[i][0] = userTable.getId().toString();
            tableValues[i][1] = userTable.getName();
            tableValues[i][2] = userTable.getPassword();
        }
        tableModel = new DefaultTableModel(tableValues, columnNames);
        table = new JTable(tableModel);

        // 设置单元格内容居中对齐
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        table.setDefaultRenderer(Object.class, centerRenderer);
        table.setEnabled(true);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);

        final JPanel panel = new JPanel();
        add(panel, BorderLayout.SOUTH);
        panel.add(new JLabel("用户名: "));
        aTextField = new JTextField("", 10);
        panel.add(aTextField);
        panel.add(new JLabel("密   码: "));
        bTextField = new JTextField("", 10);
        panel.add(bTextField);

        // 创建增删改三个按钮
        JButton addButton = new JButton("添加管理员");
        JButton updButton = new JButton("修改密码");
        JButton delButton = new JButton("注销管理员");

        //设置按钮背景为透明
        addButton.setContentAreaFilled(false);
        updButton.setContentAreaFilled(false);
        delButton.setContentAreaFilled(false);

        //添加按钮组件
        panel.add(addButton);
        panel.add(updButton);
        panel.add(delButton);

        // 添加按钮事件
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = aTextField.getText();
                String password = bTextField.getText();
                if (username.isEmpty() || password.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "请输入用户名和密码！");
                } else {
                    List<UserTable> userTable = userTableDAO.queryMulti("select * from user_table where id >= ?", UserTable.class, 1);
                    Integer id = null;
                    for (UserTable userTables : userTable) {
                        id = userTables.getId() + 1; //获取集合中的id总数
                    }
                    int update = userTableDAO.update("insert into user_table values(?,?,?)", id, username, password);
                    System.out.println(update > 0 ? "注册成功，插入数据！" : "执行没有影响表");

                    String[] rowValues = {String.valueOf(id), username, password};
                    tableModel.addRow(rowValues); // 创建表格行数据
                    aTextField.setText("");
                    bTextField.setText("");
                }
            }
        });

        //改
        updButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String userInput = aTextField.getText();
                String pwdInput = bTextField.getText();
                boolean loop = false;

                List<UserTable> userTable = userTableDAO.queryMulti("select * from user_table where id >= ?", UserTable.class, 1);
                Integer id = null;
                for (UserTable userTables : userTable) {
                    //获取集合中的id总数
                    id = userTables.getId();
                    if (userInput.equals(userTables.getName())) {
                        loop = true;
                        break;
                    }
                }
                //现在需要插入数据，如果输入框的输入的名字存在于数据表，进行修改密码的操作
                if (loop) { //当loop=true时 说名表中存在该用户，可以进行密码修改
                    int update = userTableDAO.update("update user_table set password = ? where name = ?", pwdInput, userInput);
                    System.out.println(update > 0 ? "修改成功！" : "执行没有影响表");

                    int selectedRow = table.getSelectedRow();
                    if (selectedRow != -1) {    //给表中实时添加数据
                        tableModel.setValueAt(id, selectedRow, 0);
                        tableModel.setValueAt(userInput, selectedRow, 1);
                        tableModel.setValueAt(pwdInput, selectedRow, 2);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "请输入要修改管理员的用户名！");
                }
            }
        });

        //删除数据
        delButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();
                if (selectedRow != -1) {
                    int result = JOptionPane.showConfirmDialog(null, "确定要注销该条管理员信息吗？");
                    if (result == JOptionPane.YES_OPTION) {  // 点击了“是”按钮
                        tableModel.removeRow(selectedRow);  // 从表格模型中删除选择的行
                        UserTable userTable = userTableList.get(selectedRow);   // 从数据库中删除对应的数据
                        int id = userTable.getId();
                        int update = userTableDAO.update("delete from user_table where id = ?", id);
                        System.out.println(update > 0 ? "删除成功！" : "删除失败！");

                        JOptionPane.showMessageDialog(null, "删除成功！");
                    } else if (result == JOptionPane.NO_OPTION) {  // 点击了“否”按钮
                    } else { // 点击了“取消”按钮
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "请选择要注销的用户！");
                }
            }

        });
        //密码输入框只能输入数字
        bTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!Character.isDigit(c) && c != KeyEvent.VK_BACK_SPACE) {
                    //调用事件对象的consume()禁止输入字符）
                    e.consume();
                }
            }
        });
    }
}