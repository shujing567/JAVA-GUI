package com.shujing.view.panels;


import com.shujing.common.Containers;
import com.shujing.dao.MyOrderDAO;
import com.shujing.domain.MyOrder;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author 陈书憬
 * @version 1.0
 * 用户商品信息
 */
public class P7 extends JPanel {

    private DefaultTableModel tableModel;   // 定义表格模型对象

    private JTable table;   // 表格对象
    private JButton returnButton, updButton, payButton;    // 增删改三个按钮
    MyOrderDAO myOrderDAO = new MyOrderDAO();

    public P7() {
        setLayout(new BorderLayout());

        JLabel jLabel = new Containers.JLabel_("我的订单", Font.BOLD, 25, null);

        add(jLabel, BorderLayout.NORTH);

        // 创建表头
        String[] columnNames = {"名称", "编号", "租金", "数量", "租借时间", "归还时间", "需支付", "订单状态"};
        java.util.List<MyOrder> myOrderList = myOrderDAO.queryMulti("select * from my_order where id >= ?", MyOrder.class, 1);

        // 将List<UserTable>转换为String[][]
        String[][] tableValues = new String[myOrderList.size()][8]; // 假设每个UserTable对象有3个属性
        for (int i = 0; i < myOrderList.size(); i++) {
            MyOrder myOrder = myOrderList.get(i);
            tableValues[i][0] = myOrder.getName();
            tableValues[i][1] = myOrder.getNumber();
            tableValues[i][2] = myOrder.getRent_money() + "/h";
            tableValues[i][3] = String.valueOf(myOrder.getNum());
            tableValues[i][4] = myOrder.getStart_time();
            tableValues[i][5] = myOrder.getEnd_time();
            tableValues[i][6] =myOrder.getPay() + "元";
            tableValues[i][7] = myOrder.getState();
        }
        tableModel = new DefaultTableModel(tableValues, columnNames);
        table = new JTable(tableModel);

        // 设置单元格内容居中对齐
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        table.setDefaultRenderer(Object.class, centerRenderer);
        table.setEnabled(true);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);

        final JPanel panel = new JPanel();
        add(panel, BorderLayout.SOUTH);

        returnButton = new JButton("归还电宝");
        updButton = new JButton("刷新");
        payButton = new JButton("去支付");

        //设置按钮背景为透明
        returnButton.setContentAreaFilled(false);
        updButton.setContentAreaFilled(false);
        payButton.setContentAreaFilled(false);

        panel.add(returnButton);
        panel.add(updButton);
        panel.add(payButton);

        // 归还按钮事件
        returnButton.addActionListener(new ActionListener() {
                                           @Override
                                           public void actionPerformed(ActionEvent e) {
                                               int selectedRow = table.getSelectedRow();//获取鼠标选择的行
                                               if (selectedRow != -1) { //！=-1 表示选择了表中的某一内容
                                                   SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                                   Date date = new Date();
                                                   String endTime = formatter.format(date);
                                                   System.out.println(selectedRow + 1);

                                                   MyOrder myOrder = myOrderDAO.querySingle("select * from my_order where id = ?", MyOrder.class, (selectedRow + 1));
                                                   float rentMoney = myOrder.getRent_money();
                                                   System.out.println("每小时：" + rentMoney + "元");
                                                   String startTime = myOrder.getStart_time();
                                                   System.out.println("开始租借时间：" + startTime);
                                                   System.out.println("归还时间为：" + endTime);
                                                   int num2 = myOrder.getNum();


                                                   try {
                                                       Date startDate = formatter.parse(startTime);
                                                       Date endDate = formatter.parse(endTime);

                                                       float diffInMilliSeconds = endDate.getTime() - startDate.getTime();
                                                       float diffInHours = TimeUnit.MILLISECONDS.toHours((long) diffInMilliSeconds);
                                                       //租借时间*租金
                                                       float v = diffInHours * rentMoney * num2;
                                                       System.out.println("时间差为：" + diffInHours + " 小时");

                                                       JOptionPane.showMessageDialog(null, "使用时长为：" + diffInHours + "小时，需要支付的金额为：" + v + "元");
                                                       int update = myOrderDAO.update("UPDATE my_order SET end_time = ? , pay = ?,state = ? WHERE id = ?", endTime, v, "未支付", selectedRow + 1);
                                                   } catch (Exception a) {
                                                       a.printStackTrace();
                                                   }
                                               }

                                           }
                                       }
        );

        // 刷新按钮事件
        updButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 重新查询数据库
                java.util.List<MyOrder> myOrderList = myOrderDAO.queryMulti("select * from my_order where id >= ?", MyOrder.class, 1);
                // 更新表格数据模型
                tableModel.setRowCount(0);
                for (int i = 0; i < myOrderList.size(); i++) {//清空表格后再重新添加表格
                    MyOrder myOrder = myOrderList.get(i);
                    String[] rowData = new String[8];
                    rowData[0] = myOrder.getName();
                    rowData[1] = myOrder.getNumber();
                    rowData[2] = myOrder.getRent_money() + "/h";
                    rowData[3] = String.valueOf(myOrder.getNum());
                    rowData[4] = myOrder.getStart_time();
                    rowData[5] = myOrder.getEnd_time();
                    rowData[6] = String.valueOf(myOrder.getPay()) + "元";
                    rowData[7] = myOrder.getState();

                    tableModel.addRow(rowData);//在界面添加行
                }
                table.repaint(); // 刷新表格界面
            }
        });

        payButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();//获取鼠标选择的行
                if (selectedRow != -1) { //！=-1 表示选择了表中的某一内容
                    int option = JOptionPane.showConfirmDialog(null, "请确定支付金额！");//JOptionPane 一个弹出提示框
                    if (option == JOptionPane.YES_OPTION) {
                        myOrderDAO.update("UPDATE my_order SET state = ? WHERE id = ?", "已支付", selectedRow + 1);
                    }
                }
            }
        });
    }
}