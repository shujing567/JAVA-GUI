package com.shujing.view.panels;

import com.shujing.common.Containers;
import com.shujing.dao.UserMessageTableDAO;
import com.shujing.domain.UserMessageTable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author 陈书憬
 * @version 1.0
 * 用户个人信息界面 ,可进行修改头像，修改密码，修改个人信息
 */
public class P4 extends JPanel implements Containers, ActionListener {
    private final JButton updateBtn, saveBtn, jButton2,historical_orders;
    private final TextField_ textField1, textField2, textField3, textField4, textField5;
    String username1;
    UserMessageTableDAO userMessageTableDAO = new UserMessageTableDAO();//

    public P4(String username) {
        username1 = username;//保存传入的用户名
        ImageIcon imageHead = new ImageIcon("src/images/MainHead.jpg");
        setLayout(new BorderLayout());//设置布局
        setSize(new Dimension(400, 100));
        setBackground(new Color(236, 236, 236));

        //北部
        JPanel north = new JPanel_(0, 70, new Color(244, 248, 251));
        north.setLayout(new BorderLayout());
        JLabel_ north_west = new JLabel_("    个人信息", Font.BOLD, 20, null);
        north_west.setForeground(new Color(122, 122, 122));
        north.add(north_west, BorderLayout.WEST);

        //西部 头像，修改密码
        JPanel west = new JPanel_(170, 0, new Color(253, 253, 253));
        west.setLayout(new BoxLayout(west, BoxLayout.Y_AXIS));//组件按Y轴排布

        //西部上-上  因为布局为BoxLayout，所有组件高度平均分配（在不设置值的情况下）
        JPanel_ west_north = new JPanel_(0, 0, new Color(110, 110, 110));
        west_north.setLayout(new BoxLayout(west_north, BoxLayout.Y_AXIS));
        JPanel_ west_north_north = new JPanel_(0, 0, new Color(255, 255, 255));

        //西部上-中
        JPanel_ west_north_center = new JPanel_(0, 50, new Color(253, 253, 253));
        //创建JLabel_ 用来放图片
        JLabel user_head = new JLabel_("", 0, 0, null);
        user_head.setIcon(imageHead);//将图片放入到JLabel
        user_head.setPreferredSize(new Dimension(imageHead.getIconWidth(), imageHead.getIconHeight()));     //获得图片实际像素
        west_north_center.add(user_head);

        //西部上-下
        JPanel_ west_north_south = new JPanel_(0, 0, new Color(255, 255, 255));
        JButton jButton1 = new JButton("修改头像");
        jButton1.setBackground(new Color(112, 112, 112));//设置按钮背景色
        jButton1.setForeground(new Color(234, 244, 246));//设置按钮字体颜色
        west_north_south.add(jButton1);

        //添加组件
        west_north.add(west_north_north);
        west_north.add(west_north_center);
        west_north.add(west_north_south);

        //西部下
        JPanel_ west_south = new JPanel_(0, 300, new Color(255, 255, 255));
        jButton2 = new JButton("修改密码");
        jButton2.setBackground(new Color(112, 112, 112));
        jButton2.setForeground(new Color(234, 244, 246));
        west_south.add(jButton2);

        west.add(west_north);
        west.add(west_south);

        //中间 修改信息栏
        JPanel center = new JPanel_(0, 0, new Color(246, 246, 246));
        center.setLayout(new BoxLayout(center, BoxLayout.Y_AXIS));
        JPanel_ jPanel0 = new JPanel_(0, 0, new Color(248, 248, 248));//设置一层占位
        JLabel_ jLabel0 = new JLabel_("", Font.BOLD, 15, null);
        jLabel0.setForeground(new Color(122, 122, 122));
        jPanel0.add(jLabel0);

        //查询user_message_table表 获取数据库表中相应用户的各种信息
        UserMessageTable userMessageTable = userMessageTableDAO.querySingle("select * from user_message_table where name = ?", UserMessageTable.class, username1);
        String name1 = userMessageTable.getName();
        String birthday1 = userMessageTable.getBirthday();
        String sex1 = userMessageTable.getSex();
        String address1 = userMessageTable.getAddress();
        String phone1 = userMessageTable.getPhone();

        JPanel_ jPanel1 = new JPanel_(0, 0, new Color(253, 253, 253));
        JLabel_ name = new JLabel_("昵      称： ", Font.BOLD, 15, null);
        name.setForeground(new Color(122, 122, 122));
        textField1 = new TextField_(name1, 20);
        textField1.setEnabled(false);
        jPanel1.add(name);
        jPanel1.add(textField1);

        JPanel_ jPanel2 = new JPanel_(0, 0, new Color(246, 246, 246));
        JLabel_ birthday = new JLabel_("生      日： ", Font.BOLD, 15, null);
        birthday.setForeground(new Color(122, 122, 122));
        textField2 = new TextField_(birthday1, 20);
        textField2.setEnabled(false);
        jPanel2.add(birthday);
        jPanel2.add(textField2);

        JPanel_ jPanel3 = new JPanel_(0, 0, new Color(255, 255, 255));
        JLabel_ sex = new JLabel_("性      别： ", Font.BOLD, 15, null);
        sex.setForeground(new Color(122, 122, 122));
        textField3 = new TextField_(sex1, 20);
        textField3.setEnabled(false);
        jPanel3.add(sex);
        jPanel3.add(textField3);

        JPanel_ jPanel4 = new JPanel_(0, 0, new Color(246, 246, 246));
        JLabel_ address = new JLabel_("地      址： ", Font.BOLD, 15, null);
        address.setForeground(new Color(122, 122, 122));
        textField4 = new TextField_(address1, 20);
        textField4.setEnabled(false);
        jPanel4.add(address);
        jPanel4.add(textField4);

        JPanel_ jPanel5 = new JPanel_(0, 0, new Color(255, 255, 255));
        JLabel_ phone = new JLabel_("手 机 号： ", Font.BOLD, 15, null);
        phone.setForeground(new Color(122, 122, 122));
        textField5 = new TextField_(phone1, 20);
        textField5.setEnabled(false);
        jPanel5.add(phone);
        jPanel5.add(textField5);

        JPanel_ jPanel6 = new JPanel_(0, 0, new Color(246, 246, 246));
        updateBtn = new JButton("修   改");
        west_north_south.add(updateBtn);
        saveBtn = new JButton("保   存");
        west_north_south.add(saveBtn);
        historical_orders = new JButton("历史订单查询");
        west_north_south.add(historical_orders);
        jPanel6.add(updateBtn);
        jPanel6.add(saveBtn);
        jPanel6.add(historical_orders);

        updateBtn.setContentAreaFilled(false);//设置按钮背景为透明
        saveBtn.setContentAreaFilled(false);
        historical_orders.setContentAreaFilled(false);

        JPanel_ jPanel7 = new JPanel_(0, 0, new Color(255, 255, 255));
        center.add(jPanel0);
        center.add(jPanel1);
        center.add(jPanel2);
        center.add(jPanel3);
        center.add(jPanel4);
        center.add(jPanel5);
        center.add(jPanel6);
        center.add(jPanel7);

        //添加组件
        add(north, BorderLayout.NORTH);
        add(west, BorderLayout.WEST);
        add(center, BorderLayout.CENTER);

        updateBtn.addActionListener(this);
        saveBtn.addActionListener(this);
        jButton2.addActionListener(this);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == updateBtn) {//点击修改时，文本框可编辑
            textField1.setEnabled(true);
            textField2.setEnabled(true);
            textField3.setEnabled(true);
            textField4.setEnabled(true);
            textField5.setEnabled(true);
        } else if (e.getSource() == saveBtn) {  //操作user_message_table表   获取输入的值
            String name = textField1.getText();
            String birthday = textField2.getText();
            String sex = textField3.getText();
            String address = textField4.getText();
            String phone = textField5.getText();
            int result = JOptionPane.showConfirmDialog(null, "确定要修改吗？");
            if (result == JOptionPane.YES_OPTION) {  //将值存入数据库
                userMessageTableDAO.update("UPDATE user_message_table SET name = ? , birthday = ? , sex = ?, address = ?,phone = ? WHERE name = ?", name, birthday, sex, address, phone, username1);
                textField1.setEnabled(false);
                textField2.setEnabled(false);
                textField3.setEnabled(false);
                textField4.setEnabled(false);
                textField5.setEnabled(false);
                JOptionPane.showMessageDialog(null, "修改成功！");
            } else if (result == JOptionPane.NO_OPTION) { // 点击了“否”按钮
            } else {  // 点击了“取消”按钮
            }
        } else if (e.getSource() == jButton2) {//修改密码 操作user_message_table表
            new ResetPwd(username1);//打开信息修改界面
        }
    }
}