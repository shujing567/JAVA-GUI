package com.shujing.view.panels;

import com.shujing.common.Containers;
import com.shujing.dao.CommodityDAO;
import com.shujing.domain.Commodity;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author 陈书憬
 * @version 1.0
 * 调整库存
 */
public class UpdateNum extends JFrame implements Containers, ActionListener {
    JButton jButton1, jButton2;
    TextField_ textField2;
    CommodityDAO commodityDAO = new CommodityDAO();
    String name1;


    public UpdateNum(String name) {
        name1 = name;
        Commodity commodity = commodityDAO.querySingle("select * from commodity_table where name = ?", Commodity.class, name1);
        String num = String.valueOf(commodity.getNum());

        setTitle("调整库存");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        //设置界面在屏幕居中
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        int INIT_W = 350; //窗体初始宽度
        int INIT_H = 200;  //窗体初始高度
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);

        //底部面板
        JPanel_ jPanelMain = new JPanel_(0, 0, Color.white);
        setContentPane(jPanelMain);
        jPanelMain.setLayout(new BoxLayout(jPanelMain, BoxLayout.Y_AXIS));

        JPanel jPanel1 = new JPanel();
        JLabel_ jLabel1 = new JLabel_(name1+"的库存为："+num, 0, 17, null);
        jPanel1.add(jLabel1);

        JPanel jPanel2 = new JPanel();
        JLabel_ jLabel2 = new JLabel_("调整库存为: ", 0, 13, null);
        textField2 = new TextField_("", 10);

        jPanel2.add(jLabel2);
        jPanel2.add(textField2);


        JPanel jPanel10 = new JPanel();
        jButton1 = new JButton("确定");
        jButton2 = new JButton("返回");
        jPanel10.add(jButton1);
        jPanel10.add(jButton2);

        //设置按钮背景为透明
        jButton1.setContentAreaFilled(false);
        jButton2.setContentAreaFilled(false);

        add(jPanel1);
        add(jPanel2);

        add(jPanel10);

        jButton2.addActionListener(this);
        jButton1.addActionListener(this);

        setVisible(true);
    }


    public static void main(String[] args) {
        new UpdateNum("蚂蚁充电宝");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jButton1) {
            String text = textField2.getText();
            int update = commodityDAO.update("UPDATE commodity_table SET num = ? WHERE name = ?",text,name1);
            JOptionPane.showMessageDialog(null, "已调整！");
        }
        if (e.getSource() == jButton2) {
            this.dispose();
        }
    }
}
