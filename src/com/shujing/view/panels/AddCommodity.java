package com.shujing.view.panels;

import com.shujing.common.Containers;
import com.shujing.dao.CommodityDAO;
import com.shujing.domain.Commodity;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * @author 陈书憬
 * @version 1.0
 * 管理员添加商品界面
 */
public class AddCommodity extends JFrame implements Containers, ActionListener {
    JButton jButton1, jButton2;
    boolean loop = false;
    TextField_ textField2, textField3, textField4, textField5, textField6, textField7, textField8, textField9;
    Integer id = null;
    CommodityDAO commodityDAO = new CommodityDAO();

    public AddCommodity() {
        setTitle("添加商品");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        //设置界面在屏幕居中
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        int INIT_W = 440; //窗体初始宽度
        int INIT_H = 600;  //窗体初始高度
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);

        //底部面板
        JPanel_ jPanelMain = new JPanel_(0, 0, Color.white);
        setContentPane(jPanelMain);
        jPanelMain.setLayout(new BoxLayout(jPanelMain, BoxLayout.Y_AXIS));

        JPanel jPanel1 = new JPanel();
        JLabel_ jLabel1 = new JLabel_("请输入需要添加商品的信息", 0, 17, null);
        jPanel1.add(jLabel1);

        JPanel jPanel2 = new JPanel();
        JLabel_ jLabel2 = new JLabel_("编       号: ", 0, 13, null);
        textField2 = new TextField_("", 20);
        jPanel2.add(jLabel2);
        jPanel2.add(textField2);

        JPanel jPanel3 = new JPanel();
        JLabel_ jLabel3 = new JLabel_("类       型: ", 0, 13, null);
        textField3 = new TextField_("", 20);
        jPanel3.add(jLabel3);
        jPanel3.add(textField3);

        JPanel jPanel4 = new JPanel();
        JLabel_ jLabel4 = new JLabel_("名       称: ", 0, 13, null);
        textField4 = new TextField_("", 20);
        jPanel4.add(jLabel4);
        jPanel4.add(textField4);

        JPanel jPanel5 = new JPanel();
        JLabel_ jLabel5 = new JLabel_("品       牌: ", 0, 13, null);
        textField5 = new TextField_("", 20);
        jPanel5.add(jLabel5);
        jPanel5.add(textField5);

        JPanel jPanel6 = new JPanel();
        JLabel_ jLabel6 = new JLabel_("租       金: ", 0, 13, null);
        textField6 = new TextField_("", 20);
        jPanel6.add(jLabel6);
        jPanel6.add(textField6);

        JPanel jPanel7 = new JPanel();
        JLabel_ jLabel7 = new JLabel_("站       点: ", 0, 13, null);
        textField7 = new TextField_("", 20);
        jPanel7.add(jLabel7);
        jPanel7.add(textField7);

        JPanel jPanel8 = new JPanel();
        JLabel_ jLabel8 = new JLabel_("状       态: ", 0, 13, null);
        textField8 = new TextField_("", 20);
        jPanel8.add(jLabel8);
        jPanel8.add(textField8);

        JPanel jPanel9 = new JPanel();
        JLabel_ jLabel9 = new JLabel_("是否已出租: ", 0, 13, null);
        textField9 = new TextField_("", 20);
        jPanel9.add(jLabel9);
        jPanel9.add(textField9);

        JPanel jPanel10 = new JPanel();
        jButton1 = new JButton("添加");
        jButton2 = new JButton("返回");
        jPanel10.add(jButton1);
        jPanel10.add(jButton2);

        //设置按钮背景为透明
        jButton1.setContentAreaFilled(false);
        jButton2.setContentAreaFilled(false);

        add(jPanel1);
        add(jPanel2);
        add(jPanel3);
        add(jPanel4);
        add(jPanel5);
        add(jPanel6);
        add(jPanel7);
        add(jPanel8);
        add(jPanel9);
        add(jPanel10);

        jButton2.addActionListener(this);
        jButton1.addActionListener(this);

        setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jButton2) {
            dispose();
        }
        if (e.getSource() == jButton1) {
            String text2 = textField2.getText();
            String text3 = textField3.getText();
            String text4 = textField4.getText();
            String text5 = textField5.getText();
            String text6 = textField6.getText();
            String text7 = textField7.getText();
            String text8 = textField8.getText();
            String text9 = textField9.getText();

            List<Commodity> commodities = commodityDAO.queryMulti("select * from commodity_table where id >= ?", Commodity.class, 1);
            for (Commodity commodity : commodities) {
                //获取集合中的id总数
                id = commodity.getId() + 1;
            }
            if (text2.isEmpty() || text3.isEmpty() || text4.isEmpty() || text5.isEmpty() || text6.isEmpty() || text7.isEmpty() || text8.isEmpty() || text9.isEmpty()) {
                JOptionPane.showMessageDialog(null, "请输入商品信息！");
            } else {
                int update = commodityDAO.update("insert into commodity_table values(?,?,?,?,?,?,?,?,?,?)", id, text2, text3, text4, text5, text6, text7, text8, text9, "详细");
                if (update > 0) {
                    loop = true;
                } else {
                    JOptionPane.showMessageDialog(null, "添加失败！");
                }
                if (update > 0) {
                    JOptionPane.showMessageDialog(null, "商品信息添加成功！");
                    loop = true;
                } else {
                    JOptionPane.showMessageDialog(null, "商品信息添加失败...");
                    loop = true;
                }
            }
        }
    }

    public static void main(String[] args) {
        new AddCommodity();
    }
}
