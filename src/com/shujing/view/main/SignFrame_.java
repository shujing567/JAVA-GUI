package com.shujing.view.main;

import com.shujing.common.Containers;
import com.shujing.dao.UserMessageTableDAO;
import com.shujing.domain.UserMessageTable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author 陈书憬
 * @version 1.0
 * 用户注册界面
 */
public class SignFrame_ extends JFrame implements Containers, ActionListener {
    TextField_ textFieldUser, textFieldPwd, textFieldRePwd;
    //用户输入框,密码输入框,验证码输入框
    JButton jButtonRegister = new JButton("注册");
    JButton jButtonBack = new JButton("返回");

    public SignFrame_() {
        setTitle("注册");

        //设置界面在屏幕居中
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        int INIT_W = 300; //窗体初始宽度
        int INIT_H = 300;  //窗体初始高度
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        //底部面板
        JPanel_ jPanelMain = new JPanel_(0, 0, Color.white);
        setContentPane(jPanelMain);

        //顶部空白面板
        JPanel_ jPanelNull = new JPanel_(INIT_W, 30, Color.white);

        //用户面板
        JPanel_ jPanelUser = new JPanel_(INIT_W, 50, Color.white);
        JLabel_ jLabelUser = new JLabel_("用  户  名: ", Font.BOLD, 15, Color.white);
        textFieldUser = new TextField_("", 14);
        jPanelUser.add(jLabelUser);
        jPanelUser.add(textFieldUser);

        //密码面板
        JPanel_ jPanelPwd = new JPanel_(INIT_W, 50, Color.white);
        JLabel_ jLabelPwd = new JLabel_("密         码: ", Font.BOLD, 15, Color.white);
        textFieldPwd = new TextField_("", 14);
        textFieldPwd.setEchoChar('*');
        jPanelPwd.add(jLabelPwd);
        jPanelPwd.add(textFieldPwd);

        //再次输入密码面板
        JPanel_ jPanelRePwd = new JPanel_(INIT_W, 50, Color.white);
        JLabel_ jLabelRePwd = new JLabel_("确认密码 : ", Font.BOLD, 15, Color.white);
        textFieldRePwd = new TextField_("", 14);
        textFieldRePwd.setEchoChar('*');
        jPanelRePwd.add(jLabelRePwd);
        jPanelRePwd.add(textFieldRePwd);

        //设置按钮背景透明
        jButtonRegister.setContentAreaFilled(false);
        jButtonBack.setContentAreaFilled(false);

        //按钮面板
        JPanel_ jPanelButton = new JPanel_(INIT_W, 50, Color.white);
        jPanelButton.add(jButtonRegister);
        jPanelButton.add(jButtonBack);

        //将各个面板添加到主容器
        add(jPanelNull);
        add(jPanelUser);
        add(jPanelPwd);
        add(jPanelRePwd);
        add(jPanelButton);

        //添加监听
        jButtonRegister.addActionListener(this);
        jButtonBack.addActionListener(this);

        setVisible(true);

        //密码框只能输入数字
        textFieldPwd.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!Character.isDigit(c) && c != KeyEvent.VK_BACK_SPACE) {
                    //调用事件对象的consume()禁止输入字符）
                    e.consume();
                }
            }
        });
        //确认密码框只能输入数字
        textFieldRePwd.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!Character.isDigit(c) && c != KeyEvent.VK_BACK_SPACE) {
                    //调用事件对象的consume()禁止输入字符）
                    e.consume();
                }
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jButtonRegister) {
            Register();
        } else if (e.getSource() == jButtonBack) {
            new LoginFrame_();
            dispose();
        }
    }

    public void Register() {
        //用户登录使用UserTable表  使用UserTableDAO操作
        UserMessageTableDAO userMessageTableDAO = new UserMessageTableDAO();
        //利用德鲁伊工具类，进行数据库连接，自定义sql语句 queryMulti()该方法为多行查询，
        List<UserMessageTable> userMessageTableList = userMessageTableDAO.queryMulti("select * from user_message_table where id >= ?", UserMessageTable.class, 1);
        //用来判断是否可以进行下一步
        boolean loop = false;
        //获取用户名输入框的输入值
        String username = textFieldUser.getText();
        //获取用户名密码框的输入值
        String password = textFieldPwd.getText();
        //获取密码框二次输入的值
        String rePassword = textFieldRePwd.getText();
        //id为数据库中表的数据总数
        Integer id = null;
        //遍历集合获取相应的值
        for (UserMessageTable userMessageTables : userMessageTableList) {
            //获取集合所有name的值
            String name = userMessageTables.getName();
            //获取集合所有password的值
            String pwd = userMessageTables.getPassword();
            //获取集合中的id总数
            id = userMessageTables.getId() + 1;
            //如果程序成功执行，将loop改值为true，此时说明存在该用户名
            if (username.equals(name)) {
                loop = true;
                break;
            }
        }

        //如果输入不为空
        if (!username.isEmpty()) {
            if (loop) {//如果客户输入的该用户名存在，提示"该用户名已经存在！"
                JOptionPane.showMessageDialog(null, "该用户名已经存在");
            } else {//如果输入的用户名为新的，则进行数据进行插入操作
                if (password.equals(rePassword)) {
                    //单行插入
                    int update = userMessageTableDAO.update("insert into user_message_table values(?,?,?,?,?,?,?)", id, username, password, 1, 1, 1, 1);
                    System.out.println(update > 0 ? "执行成功" : "执行没有影响表");
                    //插入成功，提示注册成功！
                    JOptionPane.showMessageDialog(null, "注册成功");
                    this.dispose();
                    new LoginFrame_();
                } else {
                    JOptionPane.showMessageDialog(null, "两次输入密码不一致！");
                }
            }
        } else {//如果不输入则弹出"请输入用户名！"
            JOptionPane.showMessageDialog(null, "请输入用户名！");
        }
    }

    public static void main(String[] args) {
        new SignFrame_();
    }
}

