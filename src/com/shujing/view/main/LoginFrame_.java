package com.shujing.view.main;

import com.shujing.common.Containers;
import com.shujing.dao.UserMessageTableDAO;
import com.shujing.dao.UserTableDAO;
import com.shujing.domain.UserMessageTable;
import com.shujing.domain.UserTable;
import com.shujing.view.panels.LoadingManager;
import com.shujing.view.panels.LoadingUser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.util.List;
import java.util.Random;

/**
 * @author 陈书憬
 * @version 1.0
 * 登录界面
 */
public class LoginFrame_ extends JFrame implements Containers, ActionListener {
    TextField_ textFieldUser, textFieldPwd, textFieldVerify; // 用户输入框,密码输入框,验证码输入框
    JButton jButtonLogin = new JButton("登录");
    JButton jButtonSign = new JButton("注册");
    JLabel_ jLabelVerifyDisplay, jLabelHead;//显示验证码  /  显示头像
    ButtonGroup radioButtonGroup = new ButtonGroup();//单选框组
    JRadioButton userRadioButton = new JRadioButton("用户");
    JRadioButton adminRadioButton = new JRadioButton("管理员");
    ImageIcon imageHead;

    public LoginFrame_() {
        setTitle("登录");
        //图片
        URL url = getClass().getResource("/images/MainHead.jpg");//从类路径的根目录开始查找
        Image image = Toolkit.getDefaultToolkit().getImage(url);
        imageHead = new ImageIcon(image);
        setDefaultCloseOperation(EXIT_ON_CLOSE);//

        //设置界面在屏幕居中
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        int INIT_W = 300; //窗体初始宽度
        int INIT_H = 400;  //窗体初始高度
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);

        //底部面板
        JPanel_ jPanelBottom = new JPanel_(0, 0, Color.white);
        setContentPane(jPanelBottom);//将jPanelBottom设置为底部面板
        setResizable(false);//设置窗体不能最大最小化

        //创建JLabel_ 用来放图片
        jLabelHead = new JLabel_("", 0, 0, null);
        jLabelHead.setIcon(imageHead);//将图片放入到JLabel
        //获得图片实际像素
        jLabelHead.setPreferredSize(new Dimension(imageHead.getIconWidth(), imageHead.getIconHeight()));

        //图片面板
        JPanel_ jPanelImg = new JPanel_(INIT_W, 100, Color.white);
        jPanelImg.add(jLabelHead, BorderLayout.SOUTH);

        //用户面板
        JPanel_ jPanelUser = new JPanel_(INIT_W, 50, Color.white);
        JLabel_ jLabelUser = new JLabel_("用户名: ", Font.BOLD, 15, Color.white);
        textFieldUser = new TextField_("", 15);
        jPanelUser.add(jLabelUser);
        jPanelUser.add(textFieldUser);

        //密码面板
        JPanel_ jPanelPwd = new JPanel_(INIT_W, 50, Color.white);
        JLabel_ jLabelPwd = new JLabel_("密    码: ", Font.BOLD, 15, Color.white);
        textFieldPwd = new TextField_("", 15);
        textFieldPwd.setEchoChar('*');//设置密码显示为*号
        jPanelPwd.add(jLabelPwd);
        jPanelPwd.add(textFieldPwd);

        //验证码面板
        JPanel_ jPanelVerify = new JPanel_(INIT_W, 50, Color.white);
        JLabel_ jLabelVerify = new JLabel_("验证码: ", Font.BOLD, 15, Color.white);
        textFieldVerify = new TextField_("", 9);
        jLabelVerifyDisplay = new JLabel_(setVerify(), Font.BOLD, 15, Color.LIGHT_GRAY);
        jLabelVerifyDisplay.setPreferredSize(new Dimension(50, 20));
        jPanelVerify.add(jLabelVerify);
        jPanelVerify.add(textFieldVerify);
        jPanelVerify.add(jLabelVerifyDisplay);

        //单选框面板
        JPanel_ jPanelCheck = new JPanel_(INIT_W, 30, Color.WHITE);
        radioButtonGroup.add(userRadioButton);
        radioButtonGroup.add(adminRadioButton);
        jPanelCheck.add(userRadioButton);
        jPanelCheck.add(adminRadioButton);

        //设置登录，注册按钮键  单选框用户和管理员 背景为透明
        jButtonLogin.setContentAreaFilled(false);
        jButtonSign.setContentAreaFilled(false);
        adminRadioButton.setContentAreaFilled(false);
        userRadioButton.setContentAreaFilled(false);

        //按钮面板
        JPanel_ jPanelButton = new JPanel_(INIT_W, 50, Color.white);
        jPanelButton.add(jButtonLogin);
        jPanelButton.add(jButtonSign);

        //将各个面板添加到主容器
        add(jPanelImg);
        add(jPanelUser);
        add(jPanelPwd);
        add(jPanelVerify);
        add(jPanelCheck);
        add(jPanelButton);

        //为按钮添加监听
        jButtonLogin.addActionListener(this);
        jButtonSign.addActionListener(this);
        userRadioButton.addActionListener(this);
        adminRadioButton.addActionListener(this);

        //显示窗口可见
        setVisible(true);

        //验证码点击事件
        jLabelVerifyDisplay.addMouseListener(new MouseAdapter() {
            //刷新验证码
            @Override
            public void mouseClicked(MouseEvent e) {
                //点击label标签时会刷新验证码
                jLabelVerifyDisplay.setText(setVerify());
            }
        });

        //密码框设置只能输入数字
        textFieldPwd.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!Character.isDigit(c) && c != KeyEvent.VK_BACK_SPACE) {
                    //调用事件对象的consume()禁止输入字符）
                    e.consume();
                }
            }
        });
    }

    //事件处理
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jButtonLogin) {
            String userType = userRadioButton.isSelected() ? "用户" : "管理员"; //获取用户选择
            login(userType);
        } else if (e.getSource() == jButtonSign) {
            dispose(); //关闭登录界面
            new SignFrame_(); //打开注册界面
        } else if (e.getSource() == adminRadioButton) {
            URL url = getClass().getResource("/images/ManagerHead.jpg");//从类路径的根目录开始查找
            Image image = Toolkit.getDefaultToolkit().getImage(url);
            imageHead = new ImageIcon(image);

            jLabelHead.setPreferredSize(new Dimension(imageHead.getIconWidth(), imageHead.getIconHeight()));
            jLabelHead.setIcon(imageHead);
        } else if (e.getSource() == userRadioButton) {
            URL url = getClass().getResource("/images/MainHead.jpg");//从类路径的根目录开始查找
            Image image = Toolkit.getDefaultToolkit().getImage(url);
            imageHead = new ImageIcon(image);
            jLabelHead.setPreferredSize(new Dimension(imageHead.getIconWidth(), imageHead.getIconHeight()));
            jLabelHead.setIcon(imageHead);
        }
    }

    //登录按钮事件处理
    public void login(String userType) {
        if (!userRadioButton.isSelected() && !adminRadioButton.isSelected()) {
            JOptionPane.showMessageDialog(null, "请选择登录身份！");
        } else {
            if (userType.equals("用户")) {//身份选择用户
                //用户登录使用UserTable表  使用UserTableDAO操作
                UserMessageTableDAO userMessageTableDAO = new UserMessageTableDAO();
                //利用德鲁伊工具类，进行数据库连接，自定义sql语句 queryMulti()该方法为多行查询，
                List<UserMessageTable> userMessageTables = userMessageTableDAO.queryMulti("select * from user_message_table where id >= ?", UserMessageTable.class, 1);
                boolean loop = false;  //用来判断是否可以进行下一步
                String username = textFieldUser.getText(); //获取用户名输入框的输入值
                String password = textFieldPwd.getText(); //获取用户名密码框的输入值
                String verify = textFieldVerify.getText();  //获取用户名验证码框的输入值
                //增强for 遍历集合获取相应的值
                for (UserMessageTable userMessageTable : userMessageTables) {
                    //获取集合所有name的值
                    String name = userMessageTable.getName();
                    //获取集合所有password的值
                    String pwd = userMessageTable.getPassword();
                    //如果程序成功执行，将loop改值为true，此时true 要开启新界面
                    if (username.equals(name) && password.equals(pwd)) {
                        loop = true;
                        break;//因为在for循环内，所以break出for循环
                    }
                }
                if (loop) { //此时loop为true时，进入下一个if语句 这个if语句出现是有可能上一个for循环里的用户名和密码不正确
                } else {//否则提示"账户密码不存在！"
                    JOptionPane.showMessageDialog(null, "账户密码不存在");
                    return;//返回
                }
                //到这一步说明账户密码输入正确，这一步如果验证码正确则进入加载界面
                if (verify.equals(jLabelVerifyDisplay.getText())) {
                    LoadingUser loadingUser = new LoadingUser(username); //进入加载页面，并且传入用户名的值
                    new Thread(loadingUser).start();
                    dispose(); //关闭当前界面
                } else { // verify.equals(jLabelVerifyDisplay.getText())与输入的不一样
                    JOptionPane.showMessageDialog(null, "验证码错误");
                }
            } else if (userType.equals("管理员")) { //身份选择管理员
                UserTableDAO userTableDAO = new UserTableDAO();  //用户登录使用UserTable表  使用UserTableDAO操作
                //利用德鲁伊工具类，进行数据库连接，自定义sql语句 queryMulti()该方法为多行查询，
                List<UserTable> userTable = userTableDAO.queryMulti("select * from user_table where id >= ?", UserTable.class, 1);
                boolean loop = false;  //用来判断是否可以进行下一步
                String username = textFieldUser.getText();  //获取用户名输入框的输入值
                String password = textFieldPwd.getText();  //获取用户名密码框的输入值
                String verify = textFieldVerify.getText();  //获取用户名验证码框的输入值
                for (UserTable userTables : userTable) {  //遍历集合获取相应的值
                    String name = userTables.getName();  //获取集合所有name的值
                    String pwd = userTables.getPassword(); //获取集合所有password的值
                    if (username.equals(name) && password.equals(pwd)) {//如果程序成功执行，将loop改值为true，此时true要开启新界面，因为在for循环内，所以break出for循环
                        loop = true;
                        break;
                    }
                }
                if (loop) { //此时loop为true时，进入下一个if语句
                } else {//否则提示"账户密码不存在！"
                    JOptionPane.showMessageDialog(null, "账户密码不存在");
                    return;
                }
                if (verify.equals(jLabelVerifyDisplay.getText())) {  //到这一步说明账户密码输入正确，这一步如果验证码正确则打开主界面
                    LoadingManager loadingManager = new LoadingManager(username); //进入加载页面，并且传入用户名的值
                    new Thread(loadingManager).start();
                    dispose(); //关闭当前界面
                } else {  // verify.equals(jLabelVerifyDisplay.getText())与输入的不一样
                    JOptionPane.showMessageDialog(null, "验证码错误");
                }
            }
        }
    }

    public String setVerify() { //用于生成验证码的方法，并返回相应数值
        //26个
        String[] str = new String[62];
        int index = 0;

        for (char c = 'A'; c <= 'Z'; c++) {
            str[index] = String.valueOf(c);
            index++;
        }
        //26个
        for (char c = 'a'; c <= 'z'; c++) {
            str[index] = String.valueOf(c);
            index++;
        }
        //10个
        for (char c = '0'; c <= '9'; c++) {
            str[index] = String.valueOf(c);
            index++;
        }


        Random random = new Random();
        int i = random.nextInt(62);//在62位设置好的数里，随机生成一个
        int j = random.nextInt(62);
        int k = random.nextInt(62);
        int z = random.nextInt(62);
        return str[i] + str[j] + str[k] + str[z];//将随机生成的四个数拼接
    }

    public static void main(String[] args) {
        new LoginFrame_();
    }
}
