package com.shujing.view.main;

import com.shujing.common.Containers;
import com.shujing.view.panels.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author 陈书憬
 * @version 1.0
 * 用户界面
 */
public class UserFrame_ extends JFrame implements Containers {
    private final JLabel_ jLabel1, jLabel2, jLabel4, jLabel3, jLabel5, jLabel6, jLabel7, jLabel8;//左边标签页
    private final JLabel jLabelClose;//缩小标签
    private final JPanel_ jPanelCenter;//中部面板，会在这个面板进行内容切换
    private final CardLayout cardLayout = new CardLayout();//卡片布局，用来进行切换内容

    public UserFrame_(String username) {
        setUndecorated(true); //设置窗口关闭栏不可见
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //设置界面显示在显示屏中间
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        //窗体初始宽度
        int INIT_W = 1200;
        //窗体初始高度
        int INIT_H = 750;
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);

        //底部面板
        JPanel_ jPanelBottom = new JPanel_(0, 0, new Color(43, 45, 48));
        jPanelBottom.setLayout(new BorderLayout());//设置底部 第一层面板布局为BorderLayout，接下来的面板可以按照东南西北中放置在其上
        setContentPane(jPanelBottom);

        //北部面板
        JPanel_ jPanelNorth = new JPanel_(INIT_W, 80, new Color(74, 139, 90));
        jPanelNorth.setLayout(new BorderLayout());
        jLabelClose = new JLabel("缩       小           ");//该标签用来缩小页面
        JLabel jLabelName = new JLabel("           你好," + username + "!");
        jPanelNorth.add(jLabelName, BorderLayout.WEST);
        jPanelNorth.add(jLabelClose, BorderLayout.EAST);

        //西部面板
        JPanel_ jPanelWest = new JPanel_(200, 400, new Color(225, 226, 220));
        jPanelWest.setLayout(new BoxLayout(jPanelWest, BoxLayout.Y_AXIS));
        jLabel1 = new JLabel_("首       页", 0, 15, null);
        jLabel2 = new JLabel_("个 人 信 息", 0, 15, null);
        jLabel3 = new JLabel_("商 品 信 息", 0, 15, null);
        jLabel4 = new JLabel_("购  物  车", 0, 15, null);
        jLabel6 = new JLabel_("在 线 客 服", 0, 15, null);
        jLabel7 = new JLabel_("我 的 订 单", 0, 15, null);
        jLabel8 = new JLabel_("历 史 订 单", 0, 15, null);
        jLabel5 = new JLabel_("退 出 界 面", 0, 15, null);

        //添加组件
        jPanelWest.add(jLabel1);
        jPanelWest.add(jLabel2);
        jPanelWest.add(jLabel3);
        jPanelWest.add(jLabel4);
        jPanelWest.add(jLabel6);
        jPanelWest.add(jLabel7);
        jPanelWest.add(jLabel8);
        jPanelWest.add(jLabel5);

        //中部面板
        jPanelCenter = new JPanel_(400, 400, new Color(255, 255, 255));
        jPanelCenter.setLayout(cardLayout);//设置卡片布局，方便内容切换
        jPanelCenter.setVisible(true);
        P0_ p0_ = new P0_(username);
        jPanelCenter.add(p0_, "p0_");//添加面板P0  欢迎用户
        P4 p4 = new P4(username);
        jPanelCenter.add(p4, "p4");//添加面板P4  个人信息
        P5 p5 = new P5();
        jPanelCenter.add(p5, "p5");//添加面板P5  商品信息
        P6 p6 = new P6();
        jPanelCenter.add(p6, "p6");//添加面板P6  购物车
        P7 p7 = new P7();
        jPanelCenter.add(p7, "p7");//添加面板P7  我的订单
        P8 p8 = new P8();
        jPanelCenter.add(p8, "p8");//添加面板P8  历史订单

        addListener();  //添加所有事件监听

        //南部面板
        JPanel_ jPanelSouth = new JPanel_(INIT_W, 50, new Color(74, 139, 90));

        //添加所有面板，并且按照布局布置相应位置
        add(jPanelNorth, BorderLayout.NORTH);
        add(jPanelWest, BorderLayout.WEST);
        add(jPanelCenter, BorderLayout.CENTER);
        add(jPanelSouth, BorderLayout.SOUTH);
        setVisible(true);
    }

    //事件监听
    private void addListener() {
        jLabelClose.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                setExtendedState(ICONIFIED);//该方法为JFrame的缩小界面的方法
            }
        });
        //下列标签为鼠标移动事件方法
        jLabel1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel1.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p0_");//当点击标签时，在中部面板jPanelCenter打开p0_面板
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel1.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel1.setForeground(new Color(153, 119, 45));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel1.setForeground(Color.black);
            }
        });
        jLabel2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel2.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p4");
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel2.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel2.setForeground(new Color(153, 119, 45));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel2.setForeground(Color.black);
            }
        });
        jLabel3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel3.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p5");
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel3.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel3.setForeground(new Color(153, 119, 45));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel3.setForeground(Color.black);
            }
        });
        jLabel4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel4.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p6");
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel4.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel4.setForeground(new Color(153, 119, 45));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel4.setForeground(Color.black);
            }
        });
        jLabel5.addMouseListener(new MouseAdapter() {  //退出界面
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel5.setForeground(Color.white);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel5.setForeground(Color.orange);
                int option = JOptionPane.showConfirmDialog(null, "确定要退出吗？");//JOptionPane 一个弹出提示框
                if (option == JOptionPane.YES_OPTION) {
                    dispose();//关闭当前界面
                    new LoginFrame_();
                } else if (option == JOptionPane.NO_OPTION) {  // 点击了“否”按钮
                } else {// 点击了“取消”按钮
                }
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel5.setForeground(new Color(153, 119, 45));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel5.setForeground(Color.black);
            }
        });
        jLabel6.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel4.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p6");
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel4.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel4.setForeground(new Color(153, 119, 45));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel4.setForeground(Color.black);
            }
        });

        jLabel7.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel7.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p7");
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel7.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel7.setForeground(new Color(153, 119, 45));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel7.setForeground(Color.black);
            }
        });
        jLabel8.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel8.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p8");
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel8.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel8.setForeground(new Color(153, 119, 45));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel8.setForeground(Color.black);
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new UserFrame_("法老");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}


