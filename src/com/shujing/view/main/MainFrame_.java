package com.shujing.view.main;

import com.shujing.common.Containers;
import com.shujing.view.panels.P0;
import com.shujing.view.panels.P1;
import com.shujing.view.panels.P2;
import com.shujing.view.panels.P3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author 陈书憬
 * @version 1.0
 * 管理员界面
 */
public class MainFrame_ extends JFrame implements Containers {
    private final JLabel_ jLabel1, jLabel2, jLabel3, jLabel4, jLabel5, jLabel6;
    private final JLabel jLabelClose, jLabelName;
    private final JPanel_ jPanelCenter, jPanelBottom, jPanelNorth, jPanelWest, jPanelSouth;
    private final CardLayout cardLayout = new CardLayout();

    public MainFrame_(String username) {
        setUndecorated(true);  //设置窗口关闭栏不可见
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //设置界面显示在显示屏中间
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        int INIT_W = 1200; //窗体初始宽度
        int INIT_H = 750; //窗体初始高度
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);

        //底部面板
        jPanelBottom = new JPanel_(0, 0, new Color(43, 45, 48));
        jPanelBottom.setLayout(new BorderLayout());
        setContentPane(jPanelBottom);

        //北部面板
        jPanelNorth = new JPanel_(INIT_W, 80, new Color(58, 103, 103));
        jPanelNorth.setLayout(new BorderLayout());
        jLabelClose = new JLabel("缩       小           ");
        jLabelName = new JLabel("           你好,管理员：" + username);
        jPanelNorth.add(jLabelClose, BorderLayout.EAST);
        jPanelNorth.add(jLabelName, BorderLayout.WEST);

        //西部面板
        jPanelWest = new JPanel_(200, 400, new Color(225, 226, 220));
        jPanelWest.setLayout(new BoxLayout(jPanelWest, BoxLayout.Y_AXIS));
        jLabel1 = new JLabel_("首       页", 0, 15, null);
        jLabel2 = new JLabel_("管 理 员 信 息", 0, 15, null);
        jLabel3 = new JLabel_("注 册 用 户 信 息", 0, 15, null);
        jLabel4 = new JLabel_("充 电 宝 管 理", 0, 15, null);
        jLabel6 = new JLabel_("订 单 信 息", 0, 15, null);
        jLabel5 = new JLabel_("退 出 界 面", 0, 15, null);

        //添加标签
        jPanelWest.add(jLabel1);
        jPanelWest.add(jLabel2);
        jPanelWest.add(jLabel3);
        jPanelWest.add(jLabel4);
        jPanelWest.add(jLabel6);
        jPanelWest.add(jLabel5);

        //中部面板
        jPanelCenter = new JPanel_(400, 400, new Color(255, 255, 255));
        jPanelCenter.setLayout(cardLayout);
        jPanelCenter.setVisible(true);
        P0 p0 = new P0();
        jPanelCenter.add(p0, "p0"); //欢迎管理员
        P1 p1 = new P1();
        jPanelCenter.add(p1, "p1"); //管理员信息
        P2 p2 = new P2();
        jPanelCenter.add(p2, "p2"); //注册用户信息
        P3 p3 = new P3();
        jPanelCenter.add(p3, "p3"); //充电宝信息管理
//        P3 p7 = new P7();
//        jPanelCenter.add(p7, "p7"); //订单信息


        //南部面板
        jPanelSouth = new JPanel_(INIT_W, 50, new Color(58, 103, 103));

        //添加面板
        add(jPanelNorth, BorderLayout.NORTH);
        add(jPanelWest, BorderLayout.WEST);
        add(jPanelCenter, BorderLayout.CENTER);
        add(jPanelSouth, BorderLayout.SOUTH);

        //添加所有事件监听
        addListener();

        setVisible(true);
    }

    private void addListener() {
        jLabelClose.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                setExtendedState(ICONIFIED);//设置缩小
            }
        });
        jLabel1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel1.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p0");//进入到欢迎管理员界面
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel1.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel1.setForeground(new Color(106, 138, 8));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel1.setForeground(Color.black);
            }
        });
        jLabel2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel2.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p1");//进入到管理员信息界面
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel2.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel2.setForeground(new Color(106, 138, 8));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel2.setForeground(Color.black);
            }
        });
        jLabel3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel3.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p2");//进入到注册用户信息
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel3.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel3.setForeground(new Color(106, 138, 8));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel3.setForeground(Color.black);
            }
        });
        jLabel4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel4.setForeground(Color.white);
                cardLayout.show(jPanelCenter, "p3");//注册商品信息
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel4.setForeground(Color.orange);
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel4.setForeground(new Color(106, 138, 8));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel4.setForeground(Color.black);
            }
        });
        jLabel5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jLabel5.setForeground(Color.white);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                jLabel5.setForeground(Color.orange);
                int option = JOptionPane.showConfirmDialog(null, "确定要退出吗？");
                if (option == JOptionPane.YES_OPTION) {
                    dispose();//点击退出关闭当前界面
                    new LoginFrame_();
                } else if (option == JOptionPane.NO_OPTION) {  // 点击了“否”按钮
                } else {  // 点击了“取消”按钮
                }
            }

            public void mouseEntered(MouseEvent evt) {
                jLabel5.setForeground(new Color(106, 138, 8));
            }

            public void mouseExited(MouseEvent evt) {
                jLabel5.setForeground(Color.black);
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new MainFrame_("1");
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }
}


