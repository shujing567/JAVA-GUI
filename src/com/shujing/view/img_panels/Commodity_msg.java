package com.shujing.view.img_panels;

import com.shujing.common.Containers;
import com.shujing.dao.CommodityDAO;
import com.shujing.domain.Commodity;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;


/**
 * @author 陈书憬
 * @version 1.0
 */
public class Commodity_msg extends JFrame implements Containers, ActionListener {
    TextField_ textField2, textField3, textField4, textField5, textField6,
            textField7, textField8, textField9, textField10, textField11,
            textField12, textField13;
    URL url;

    public Commodity_msg(String name, int i) {
        String name1 = name;
        int select = i;
        System.out.println(name);
        CommodityDAO commodityDAO = new CommodityDAO();
        //1. 查询
        Commodity commodity = commodityDAO.querySingle("select * from commodity_table where name = ?", Commodity.class, name1);


        setTitle("产品详细");
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        int INIT_W = 700; //窗体初始宽度
        int INIT_H = 520;  //窗体初始高度
        setBounds(p.x - INIT_W / 2, p.y - INIT_H / 2, INIT_W, INIT_H);

        setLayout(new GridLayout(1, 2));

        JPanel left = new JPanel();
        left.setBackground(new Color(236, 236, 236));

        JPanel img_jPanle = new JPanel();
        if (select == 1) {
            url = getClass().getResource("/images/commodity/c1.png");//从类路径的根目录开始查找
        } else if (select == 2) {
            url = getClass().getResource("/images/commodity/c2.png");//从类路径的根目录开始查找
        } else if (select == 3) {
            url = getClass().getResource("/images/commodity/c3.png");//从类路径的根目录开始查找
        } else if (select == 4) {
            url = getClass().getResource("/images/commodity/c4.png");//从类路径的根目录开始查找
        }


        Image image = Toolkit.getDefaultToolkit().getImage(url);
        ImageIcon imageHead = new ImageIcon(image);
        //创建JLabel_ 用来放图片
        JLabel_ label_msg = new JLabel_("", 0, 0, null);
        label_msg.setIcon(imageHead);//将图片放入到JLabel
        //获得图片实际像素
        label_msg.setPreferredSize(new Dimension(imageHead.getIconWidth(), imageHead.getIconHeight()));
        img_jPanle.add(label_msg);

        left.add(img_jPanle);


        JPanel r_left = new JPanel();
        r_left.setBackground(new Color(253, 253, 253));
        r_left.setLayout(new GridLayout(13, 1));

        JPanel jPanel1 = new JPanel();
        JLabel_ jLabel1 = new JLabel_("产品参数", 0, 17, null);
        jPanel1.add(jLabel1);


        JPanel jPanel2 = new JPanel();
        JLabel_ jLabel2 = new JLabel_("产品编号: ", 0, 13, null);
        textField2 = new TextField_("", 13);
        jPanel2.add(jLabel2);
        jPanel2.add(textField2);

        JPanel jPanel3 = new JPanel();
        JLabel_ jLabel3 = new JLabel_("产品名称: ", 0, 13, null);
        textField3 = new TextField_("", 13);
        jPanel3.add(jLabel3);
        jPanel3.add(textField3);

        JPanel jPanel4 = new JPanel();
        JLabel_ jLabel4 = new JLabel_("产品类型: ", 0, 13, null);
        textField4 = new TextField_("", 13);
        jPanel4.add(jLabel4);
        jPanel4.add(textField4);

        JPanel jPanel5 = new JPanel();
        JLabel_ jLabel5 = new JLabel_("产品品牌: ", 0, 13, null);
        textField5 = new TextField_("", 13);
        jPanel5.add(jLabel5);
        jPanel5.add(textField5);

        JPanel jPanel6 = new JPanel();
        JLabel_ jLabel6 = new JLabel_("所在站点: ", 0, 13, null);
        textField6 = new TextField_("", 13);
        jPanel6.add(jLabel6);
        jPanel6.add(textField6);

        JPanel jPanel7 = new JPanel();
        JLabel_ jLabel7 = new JLabel_("产品状态: ", 0, 13, null);
        textField7 = new TextField_("", 13);
        jPanel7.add(jLabel7);
        jPanel7.add(textField7);


        JPanel jPanel8 = new JPanel();
        JLabel_ jLabel8 = new JLabel_("电池能量: ", 0, 13, null);
        textField8 = new TextField_("", 13);
        jPanel8.add(jLabel8);
        jPanel8.add(textField8);

        JPanel jPanel9 = new JPanel();
        JLabel_ jLabel9 = new JLabel_("额定容量：", 0, 13, null);
        textField9 = new TextField_("", 13);
        jPanel9.add(jLabel9);
        jPanel9.add(textField9);

        JPanel jPanel10 = new JPanel();
        JLabel_ jLabel10 = new JLabel_("输入接口：", 0, 13, null);
        textField10 = new TextField_("", 13);
        jPanel10.add(jLabel10);
        jPanel10.add(textField10);

        JPanel jPanel11 = new JPanel();
        JLabel_ jLabel11 = new JLabel_("输出接口：", 0, 13, null);
        textField11 = new TextField_("", 13);
        jPanel11.add(jLabel11);
        jPanel11.add(textField11);

        JPanel jPanel12 = new JPanel();
        JLabel_ jLabel12 = new JLabel_("产品尺寸：", 0, 13, null);
        textField12 = new TextField_("", 13);
        jPanel12.add(jLabel12);
        jPanel12.add(textField12);


        JPanel jPanel13 = new JPanel();
        JLabel_ jLabel13 = new JLabel_("充电时间：", 0, 13, null);
        textField13 = new TextField_("", 13);
        jPanel13.add(jLabel13);
        jPanel13.add(textField13);

        r_left.add(jPanel1);
        r_left.add(jPanel2);
        r_left.add(jPanel3);
        r_left.add(jPanel4);
        r_left.add(jPanel5);
        r_left.add(jPanel6);
        r_left.add(jPanel7);
        r_left.add(jPanel8);
        r_left.add(jPanel9);
        r_left.add(jPanel10);
        r_left.add(jPanel11);
        r_left.add(jPanel12);
        r_left.add(jPanel13);


        add(left);
        add(r_left);

        textField2.setText(commodity.getNumber());
        textField3.setText(commodity.getName());
        textField4.setText(commodity.getTypes());
        textField5.setText(commodity.getBrand());
        textField6.setText(commodity.getLocations());
        textField7.setText(commodity.getState());
        textField8.setText(commodity.getEnergy());
        textField9.setText(commodity.getEnergy_size());
        textField10.setText(commodity.getInput());
        textField11.setText(commodity.getOutput());
        textField12.setText(commodity.getSize());
        textField13.setText(commodity.getSize());

        textField2.setEnabled(false);
        textField3.setEnabled(false);
        textField4.setEnabled(false);
        textField5.setEnabled(false);
        textField6.setEnabled(false);
        textField7.setEnabled(false);
        textField8.setEnabled(false);
        textField9.setEnabled(false);
        textField10.setEnabled(false);
        textField11.setEnabled(false);
        textField12.setEnabled(false);
        textField13.setEnabled(false);


        setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {

    }


//    public static void main(String[] args) {
//        new Commodity_msg("1");
//    }
}
