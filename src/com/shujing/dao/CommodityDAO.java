package com.shujing.dao;

import com.shujing.domain.Commodity;

/**
 * @author 陈书憬
 * @version 1.0
 * 用于操作商品信息表
 */
public class CommodityDAO extends BasicDAO<Commodity>{
}
