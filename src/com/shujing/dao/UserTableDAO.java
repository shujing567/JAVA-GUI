package com.shujing.dao;



import com.shujing.domain.UserTable;

/**
 * @author
 * @version 1.0
 * 在这里对user_table 进行操作
 */
public class UserTableDAO extends BasicDAO<UserTable> {
    //1. 就有 BasicDAO 的方法
    //2. 根据业务需求，可以编写特有的方法.
}
