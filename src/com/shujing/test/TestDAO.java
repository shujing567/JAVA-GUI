//package com.shujing.test;
//
//
//import com.shujing.dao.UserTableDAO;
//import com.shujing.domain.UserTable;
//import org.junit.jupiter.api.Test;
//
//import java.util.List;
//
///**
// * @author 陈书憬
// * @version 1.0
// * 仅参考
// */
//public class TestDAO {
//
//    //测试ActorDAO 对actor表crud操作
//    @Test
//    public void testActorDAO() {
//
//        UserTableDAO actorDAO = new UserTableDAO();
//        //1. 查询
//        List<UserTable> actors = actorDAO.queryMulti("select * from actor where id >= ?", UserTable.class, 1);
//        System.out.println("===查询结果===");
//        for (UserTable userTable : actors) {
//            System.out.println(userTable);
//        }
//
//        //2. 查询单行记录
//        UserTable actor = actorDAO.querySingle("select * from actor where id = ?", UserTable.class, 6);
//        System.out.println("====查询单行结果====");
//        System.out.println(actor);
//
//        //3. 查询单行单列
//        Object o = actorDAO.queryScalar("select name from actor where id = ?", 6);
//        System.out.println("====查询单行单列值===");
//        System.out.println(o);
//
//        //4. dml操作  insert ,update, delete
//        int update = actorDAO.update("insert into actor values(null, ?, ?, ?, ?)", "w", "男", "2000-11-11", "999");
//
//        System.out.println(update > 0 ? "执行成功" : "执行没有影响表");
//
//
//    }
//}
