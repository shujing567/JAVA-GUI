package com.shujing.common;

import javax.swing.*;
import java.awt.*;

/**
 * @author 陈书憬
 * @version 1.0
 * 容器接口 用来设置各个组件的方法值
 */
public interface Containers {

    class JPanel_ extends JPanel {
        public JPanel_(int weight, int height, Color bgColor) {
            setOpaque(true);
            setPreferredSize(new Dimension(weight, height));
            setBackground(bgColor);
        }
    }

    class JLabel_ extends JLabel {
        public JLabel_(String text, int fonts, int fontsize, Color color) {
            setText(text);
            setOpaque(true);
            setFont(new Font("宋体", fonts, fontsize));
            setBackground(color);
//            setForeground(color);
            setHorizontalAlignment(JLabel.CENTER);
            setAlignmentX(Component.CENTER_ALIGNMENT);
            setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0)); // Add top and bottom spacing

        }
    }

    class TextField_ extends TextField {
        public TextField_(String text, int columns) {
            setText(text);
            setColumns(columns);
        }
    }
}
