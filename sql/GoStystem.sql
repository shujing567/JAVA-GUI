/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.19 : Database - db_system
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_system` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `db_system`;

/*Table structure for table `commodity_table` */

DROP TABLE IF EXISTS `commodity_table`;

CREATE TABLE `commodity_table` (
  `id` int(10) NOT NULL,
  `number` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `types` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `brand` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `rent_money` double DEFAULT NULL,
  `locations` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `rent` varchar(40) COLLATE utf8_bin DEFAULT '否',
  `input` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `output` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `size` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `energy` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `energy_size` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `time` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `battery` int(40) DEFAULT NULL,
  `num` int(40) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `commodity_table` */

insert  into `commodity_table`(`id`,`number`,`types`,`name`,`brand`,`rent_money`,`locations`,`state`,`rent`,`input`,`output`,`size`,`energy`,`energy_size`,`time`,`battery`,`num`) values 
(1,'03310117112057','A类','蚂蚁充电宝','蚂蚁牌',1,'A站点','正常','否','USB-C','USB-A / USB-C','147.9 × 71.6 × 18.4mm','37Wh 3.7V 10000mAh\r','6000mAh（5V / 3A）\r','约4小时',100,8),
(2,'03310117111111','B类','大象充电宝','大象牌',2,'B站点','正常','否','2','USB-A / USB-C','147.9 × 71.6 × 18.4mm','37Wh 3.7V 10000mAh\r','6000mAh（5V / 3A）\r','约5小时',100,8),
(3,'40040117111111','C类','超能充电宝','超级牌',3,'C站点','正常','否','3','USB-A / USB-C','147.9 × 71.6 × 18.4mm','37Wh 3.7V 10000mAh\r','6000mAh（5V / 3A）\r','约3小时',61,8),
(4,'56640117111111','D类','粑粑充电宝','沸羊羊牌',4,'D站点','正常','否','4','USB-A / USB-C','147.9 × 71.6 × 18.4mm','37Wh 3.7V 10000mAh\r','6000mAh（5V / 3A）\r','约4小时',38,9);

/*Table structure for table `my_order` */

DROP TABLE IF EXISTS `my_order`;

CREATE TABLE `my_order` (
  `id` int(10) NOT NULL,
  `name` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `rent_money` double DEFAULT NULL,
  `num` int(100) DEFAULT NULL,
  `number` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `start_time` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `end_time` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(40) COLLATE utf8_bin DEFAULT '未支付',
  `pay` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `my_order` */

/*Table structure for table `user_message_table` */

DROP TABLE IF EXISTS `user_message_table`;

CREATE TABLE `user_message_table` (
  `id` int(32) NOT NULL,
  `name` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `sex` char(10) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `birthday` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `user_message_table` */

insert  into `user_message_table`(`id`,`name`,`password`,`sex`,`address`,`birthday`,`phone`) values 
(1,'1','1','男','广东省广州市','2013-01-01','18589978547');

/*Table structure for table `user_shop_table` */

DROP TABLE IF EXISTS `user_shop_table`;

CREATE TABLE `user_shop_table` (
  `id` int(10) NOT NULL,
  `name` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `rent_money` double DEFAULT NULL,
  `num` int(100) DEFAULT NULL,
  `number` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `user_shop_table` */

/*Table structure for table `user_table` */

DROP TABLE IF EXISTS `user_table`;

CREATE TABLE `user_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_bin NOT NULL,
  `password` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `user_table` */

insert  into `user_table`(`id`,`name`,`password`) values 
(1,'1','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
