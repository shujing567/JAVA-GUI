# GUI课程设计-Java

#### 介绍
1. 用Swing界面实现一个简单的系统（充电宝租赁系统），分为管理端和客户端。
2. 管理端：对产品可以进行增添，删除，更新产品信息，查看当前产品信息，并且对普通用户可以进行注销操作，模拟充电宝充电效果，对库存进行调整。
3. 客户端：对个人信息进行修改，可以租借不同类型的充电宝，假设当前电宝电量不足百分之五十会提示电量小于百分之五十，不能租借，成功租借的电宝添入购物车，最后租借成功会形成订单，用户在一定时间后归还，系统根据时间对用户收费，用户点击去支付，完成模拟支付。订单完成后，会形成历史订单。
（当前项目并不完善，有很多的bug，包括数据库表格的设计，一开始并未考虑每张表的作用，都是边做边设计，有很多问题在设计当时并未解决，只是有了个形状，刚开始学习...一开始以为一个用户一张数据库表...需要的可以根据自己实际情况进行修改。）

#### 软件架构
软件架构说明

1.  项目/images： 图片存储
1.  项目/src/com/shujing/view: 负责与用户交互,处理业务逻辑。
2.  项目/src/com/shujing/common: 通用类接口，实现自定义组件内容。
3.  项目/src/com/shujing/dao: 与数据库进行交互，包括数据的读取、写入和持久化等操作。
4.  项目/src/com/shujing/domain: 数据库表格对应实体类。
5.  项目/src/com/shujing/jdbc: 基于druid数据库连接池的工具类。
6.  项目/src/com/shujing/common: 通用类接口，实现自定义组件内容。
7.  项目/druid.properties 德鲁伊连接池配置
8.  项目/mysql.properties 数据库配置

1.  表现层（Presentation Layer）： 负责与用户交互，提供GUI界面，采用Swing实现。
2.  业务逻辑层（Business Logic Layer）： 负责处理业务逻辑，包括对用户输入的处理、数据计算和业务规则的实现。
3.  数据访问层（Data Access Layer）： 负责与数据库进行交互，包括数据的读取、写入和持久化等操作。使用（选择的数据库，如MySQL）作为数据存储介质。
4.  连接池层（Connection Pool Layer）： 使用德鲁伊连接池（Druid Connection Pool）来管理数据库连接，提高系统性能和稳定性。
![输入图片说明](https://foruda.gitee.com/images/1713681588903929940/9e715ab3_14137986.png "屏幕截图")

#### 安装教程

1.  安装即用

#### 使用说明
1.  JDK1.7
1.  下载后打开项目
2.  导入项目内的sql文件
3.  修改properties配置文件信息,数据库名,用户名,密码
4.  在main包打开打开MainFrame_运行

#### db_system            数据库表
1. user_table             管理员表
2. user_message_table     普通用户表
3. my_order               订单表
4. user_shop_table        购物车表
5. commodity_table        商品表
（实际上管理员和普通用户只需要一张表）


#### 效果图：
![输入图片说明](https://foruda.gitee.com/images/1713681954656305482/16becdd9_14137986.png "图片1.png")
![输入图片说明](https://foruda.gitee.com/images/1713681973021598940/42bb38db_14137986.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1713681985968309745/eb9d4c96_14137986.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1713681986100484786/b8b0522c_14137986.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1713681997618628136/0adf16a1_14137986.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1713682016739383354/c17a32ea_14137986.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1713682016720954418/8c356cd7_14137986.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1713682017343673093/ca792b00_14137986.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1713682017391396535/d9641241_14137986.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1713682017317662773/ffa76c5c_14137986.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1713682017447629107/acab8edb_14137986.png "屏幕截图")